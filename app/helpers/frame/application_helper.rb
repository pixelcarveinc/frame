module Frame
  module ApplicationHelper
  
    def cms_css_namespace
      content_for(:cms_css_namespace) || 'backend frame'
    end
    
    def breadcrumb(input_page)
      title = 'Page // '
      Frame::Link.all.each do |p|
        if p.page.title == input_page.title && !p.is_root?
          p.ancestors.each do |a|
            title += link_to a.page.title, edit_page_path(a)
            title += ' // '
          end
        end
      end
      title += input_page.title
      title = title.html_safe
    end
  end
end
