module Frame
  class MessageMailer < ActionMailer::Base
    
    def message_email(message)
      emails = message.contact.emails.split(',')
      @message = message
      
      mail to: emails, subject: 'New Message From Contact Form'
    end
  end
end
