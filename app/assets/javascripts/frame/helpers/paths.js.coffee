

_paths = 
  links: ''
  pages: ''

pxcv.addHelpers

  setFramePaths: (paths) -> _paths = _.clone paths

  frameLinksPath: -> _paths.links

  frameLinkPath: (id) -> "#{_paths.links}/#{id}"

  newFrameLinkPath: (id) -> @frameLinkPath(id) + '/new'

  editFrameLinkPath: (id) -> @frameLinkPath(id) + '/edit'

  framePagesPath: -> _paths.pages
  
  framePagePath: (id) -> "#{_paths.pages}/#{id}"
  
  newFramePagePath: (id) -> @framePagePath(id) + '/new'
  
  editFramePagePath: (id) -> @framePagePath(id) + '/edit'
  
  frameGalleryPath: (id) -> @framePagePath(id).replace(/pages/, 'galleries')
  
  frameGalleryImagesPath: (id) -> @frameGalleryPath(id) + '/images'
  
  frameGalleryImagePath: (galleryId, imageId) ->
    @frameGalleryImagesPath(galleryId) + "/#{imageId}"
  
  newFrameGalleryImagePath: (galleryId, imageId) ->
    @frameGalleryImagePath(galleryId, imageId) + '/new'
  
  editFrameGalleryImagePath: (galleryId, imageId) ->
    @frameGalleryImagePath(galleryId, imageId) + '/edit'

  frameBlogPath: (id) -> @framePagePath(id).replace(/pages/, 'blogs')
  
  frameBlogPostsPath: (id) -> @frameBlogPath(id) + '/posts'
  
  frameBlogPostPath: (blogId, postId) ->
    @frameBlogPostsPath(blogId) + "/#{postId}"
  
  newFrameBlogPostPath: (blogId, postId) ->
    @frameBlogPostPath(blogId, postId) + '/new'
  
  editFrameBlogPostPath: (blogId, postId) ->
    @frameBlogPostPath(blogId, postId) + '/edit'
    
    
