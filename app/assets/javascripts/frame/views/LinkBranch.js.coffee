#= require frame/namespace


class window.chocolate.frame.views.LinkBranch extends Backbone.View

  tagName: 'li'
  className: 'branch'

  events:
    'click a.fa-plus-square': '_expand'
    'click a.fa-minus-square': '_collapse'
    'confirm:complete a.icon-delete': '_destroy'


  template: JST['frame/branch']

  initialize: ->

    @$el.data 'view', this


  render: ->



    @cleanUp()

    @$el.html @template _.extend @model.toJSON(), confirm: @confirmMessage()

    @$children = @$ 'ul.children'
    @$dropzone = @$ '.dropzone'

    @model.get('children').each (m) =>
      v = new chocolate.frame.views.LinkBranch model: m
      @listenTo v, 'drag', @_dragRT
      @listenTo v, 'startDrag', @_startDragRT
      @listenTo v, 'stopDrag', @_stopDragRT
      @listenTo v, 'drop', @_dropRT
      @listenTo v, 'destroy', @_destroyRT
      @listenTo v, 'slyresize', @_resizeRT
      @children.push v
      @$children.append v.render().el

    @$el.draggable
      cursorAt:
        top: 5
        left: -5
      appendTo: 'body'
      handle: '> .bar'
      cancel: 'a, button'
      opacity: 0.8
      helper: 'clone'
      scroll: false
      zIndex: 100
      refreshPositions: true
      start: @_startDrag
      stop: @_stopDrag
      drag: _.throttle(@_drag, 100)

    @$dropzone.droppable
      accept: 'li.branch, li.page'
      tolerance: 'pointer'
      activeClass: 'drop-active'
      hoverClass: 'drop-hover'
      greedy: true
      drop: @_drop

    @_rendered = true

    @collapsible()



    @

  cleanUp: ->
    if @_rendered
      c.remove() for c in @children
      @$el.draggable 'destroy'
      @$dropzone.droppable 'destroy'

    @children = []
    @_rendered = false

  remove: ->
    @cleanUp()
    super

  confirmMessage: ->
    title = @model.get 'title'
    confirm = ''
    if @model.get('children').length > 0
      confirm = "<b>WARNING:</b> All links under this link will be deleted as well.<br/><br/>"
    confirm += "Are you sure you wish to delete this link to #{title}?<br/><br/>"
    confirm += "<b>NOTE:</b> This will not delete the page and it's content."

  collapsible: ->
    if @$el.find(".children li").length > 0
      @$el.find(".toggle-children").first().addClass("active")
      @$el.find(".toggle-children").first().removeClass("fa-minus-square")
      @$el.find(".toggle-children").first().addClass("fa-plus-square")
    else
      @$el.find(".toggle-children").first().removeClass("active")



  expand: (id) ->
    el = $('.id[data-id="' + id + '"]')

    setTimeout(=>
      el.parents(".children").show()
      #el.closest(".bar").effect("highlight", {color: '#D2C6BD'})
      @trigger 'slyresize', @
    ,500)

    el.closest(".branch").find(".children").first().fadeIn('fast', =>
      
      @trigger 'slyresize', @

    )
    el.closest(".branch").find('.fa-plus-square').first().removeClass('fa-plus-square').addClass("fa-minus-square")
    $.each el.closest(".branch").parents(".branch"), (i, el) ->
      $(el).find(".toggle-children").first().removeClass('fa-plus-square').addClass("fa-minus-square")


  _expand: (e, result) =>
    e.preventDefault()
    e.stopPropagation()

    el = e.currentTarget



    children = $(el).closest(".branch").find(".children")
    id = $(el).closest(".branch").find(".id").data("id")

    $.cookie('expand-id', id)

    if children.find("li").length == 0
      return false

    $(el).removeClass("fa-plus-square")
    $(el).addClass("fa-minus-square")


    children.first().fadeIn('fast', =>

      
      @trigger 'slyresize', @

    )




  _collapse: (e, result) =>

    unless ($(e.currentTarget).hasClass("active"))
      return false

    e.preventDefault()
    e.stopPropagation()

    el = e.currentTarget

    $(el).addClass("fa-plus-square")
    $(el).removeClass("fa-minus-square")

    $(el).closest(".branch").find(".children").first().fadeOut('fast', =>

      
      @trigger 'slyresize', @


    )





  _destroy: (e, result) ->

    return unless result and $(e.target).closest('li.branch')[0] is @el

    if @$el.closest(".branch").parents(".branch").first().find(".children li").length == 1
      @$el.closest(".branch").parents(".branch").first().find(".toggle-children").first().removeClass("active")
      @$el.closest(".branch").parents(".branch").first().find(".toggle-children").first().removeClass("fa-plus-square")
      @$el.closest(".branch").parents(".branch").first().find(".toggle-children").first().addClass("fa-minus-square")


    @$el.slideUp =>
      @remove()
      @trigger 'destroy', @

    @model.destroy()





  _startDrag: =>
    @$el.addClass 'active'
    @trigger 'startDrag', @, arguments...

  _stopDrag: =>
    @$el.removeClass 'active'
    @trigger 'stopDrag', @, arguments...

  _drag: =>
    @trigger 'drag', @, arguments...

  _drop: =>
    @trigger 'drop', @, arguments...


    if @$el.closest(".branch").find(".children li").length == 1
      @$el.closest(".branch").find(".toggle-children").first().addClass("active")
      @$el.closest(".branch").find(".toggle-children").first().addClass("fa-minus-square")
      @$el.closest(".branch").find(".toggle-children").first().removeClass("fa-plus-square")
      @$el.closest(".branch").find(".children").fadeIn()

  # retrigger children

  _dragRT: =>
    @trigger 'drag', arguments...

  _startDragRT: =>
    @trigger 'startDrag', arguments...

  _stopDragRT: =>
    @trigger 'stopDrag', arguments...

  _dropRT: =>
    @trigger 'drop', arguments...

  _resizeRT: =>
    @trigger 'slyresize', arguments...

  _destroyRT: =>
    @trigger 'destroy', arguments...
