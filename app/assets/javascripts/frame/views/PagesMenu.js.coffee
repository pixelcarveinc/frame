#= require frame/namespace


class window.chocolate.frame.views.PagesMenu extends Backbone.View

  events:
    'keyup #search-page': '_filterPages'

  initialize: ->

    @collection = new chocolate.frame.models.Pages @options.pages

    @$frame     = @$ '.frame'
    @$scrollbar = @$ '.scrollbar'
    @$pages     = @$ 'ul.pages'
    @$footer    = @$ '.footer'

    @pages = for page in @$pages.find('li.page')
      v = new chocolate.frame.views.PageBlock
        el:    page
        model: @collection.get $(page).data('id')
      @listenTo v, 'drag',      @_dragRT
      @listenTo v, 'startDrag', @_startDragRT
      @listenTo v, 'stopDrag',  @_stopDragRT
      v

    # init sly scroller
    @sly = new Sly @$frame,
      scrollBy:      100
      speed:         300
      scrollBar:     @$scrollbar
      dragHandle:    1
      dynamicHandle: 1
      clickBar:      1
    @sly.init()

    # resize handler for global layout changes
    $(window).on 'resize', _.debounce(@_resize, 100)
    @_resize()

  _resize: =>
    @$frame.height @$footer.outerOffset().top - @$frame.outerOffset().top

    if @$frame.height() >= @$pages.height()
      @$scrollbar.hide()
    else
      @$scrollbar.show()

    @$scrollbar.height @$frame.height()
    @sly.reload()

  remove: ->
    @cleanUp()
    # not calling super on purpose

  cleanUp: ->
    @sly.destroy()
    p.remove() for p in @pages

  # retrigger drag events

  _dragRT: =>
    @trigger 'drag', arguments...

  _startDragRT: =>
    @trigger 'startDrag', arguments...

  _stopDragRT: =>
    @trigger 'stopDrag', arguments...

  _filterPages: (e) ->
    el = e.currentTarget

    valThis = $(el).val().toLowerCase()
    if valThis == ''
      $('ul.pages > li').show()
    else
      $('ul.pages > li').each (i, el) ->
        text = $(el).find(".title").text().toLowerCase()
        if text.indexOf(valThis) >= 0 then $(el).show() else $(el).hide()
