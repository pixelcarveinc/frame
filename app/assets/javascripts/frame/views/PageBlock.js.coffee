# require frame/namespace


class window.chocolate.frame.views.PageBlock extends Backbone.View

  initialize: ->


    @$el.data 'view', this

    @$el.draggable
      cursorAt:
        top: 5
        left: -5
      appendTo: 'body'
      cancel: 'a, button'
      opacity: 0.8
      helper: 'clone'
      scroll: false
      zIndex: 100
      refreshPositions: true
      start: @_startDrag
      stop: @_stopDrag
      drag: _.throttle(@_drag, 100)

  cleanUp: ->
    @$el.draggable 'destroy'

  remove: ->
    @cleanUp()
    # not calling super on purpose

  _startDrag: =>
    @$el.addClass 'active'
    @trigger 'startDrag', this, arguments...

  _stopDrag: =>
    @$el.removeClass 'active'
    @trigger 'stopDrag', this, arguments...

  _drag: =>
    @trigger 'drag', this, arguments...
