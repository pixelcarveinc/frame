#= require frame/namespace
#= require frame/models/Link
#= require frame/models/Links

Link  = chocolate.frame.models.Link
Links = chocolate.frame.models.Links


class window.chocolate.frame.views.LinksTree extends Backbone.View


  initialize: ->
    _.extend @, chocolate.style.views.utils.SlyResizeMixin


    @collection = new Links @options.links

    @$frame     = @$ '.frame'
    @$scrollbar = @$ '.scrollbar'
    @$roots = @$content = @$ 'ul.roots'

    @pagesMenu = new chocolate.frame.views.PagesMenu _.extend {}, @options, el: '#context-menu'
    @listenToDrag @pagesMenu

    @roots = for m in @collection.models
      v = @createBranch(m)
      @$roots.append v.el
      v

    if $.cookie('expand-id') != undefined
      v.expand($.cookie('expand-id'))

    @sly = new Sly @$frame,
      scrollBy:      100
      speed:         300
      scrollBar:     @$scrollbar
      dragHandle:    1
      dynamicHandle: 1
      clickBar:      1
    @sly.init()

    @setupDropzone() if @collection.length is 0
    @listenTo @collection, 'destroy', @_checkIfNoRoots

    $(window).on 'resize', _.debounce(@resize, 100)
    $(document).on 'page:before-change', @remove

    @resize()


  resize: =>
    
    @resizeSly()

  cleanUp: =>
    @$el.droppable('destroy') if @$el.hasClass 'ui-droppable'
    @sly.destroy()
    _.each(@roots, (v) -> v.remove())
    @pagesMenu.remove()

  remove: =>
    @cleanUp()
    delete chocolate.frame.links
    super

  createBranch: (model) ->
    v = new chocolate.frame.views.LinkBranch model: model
    @listenToDrag v
    @listenTo v, 'slyresize', @resize
    @listenTo v, 'destroy', @resize
    

    v.render()


  listenToDrag: (view) ->
    @listenTo view, 'drag',      @_drag
    @listenTo view, 'startDrag', @_startDrag
    @listenTo view, 'drop',      @_drop

  setupDropzone: ->
    @$el.droppable
      accept: 'li.branch, li.page'
      tolerance: 'pointer'
      activeClass: 'drop-active'
      hoverClass: 'drop-hover'
      greedy: true
      drop: => @trigger 'drop', @, arguments...
    @listenTo @, 'drop', @_drop

  insertRoot: (target) ->
    @$roots.append target.$el
    @roots.push target
    @stopListening @, 'drop', @_drop
    @$el.droppable('destroy') if @$el.hasClass 'ui-droppable'

  _checkIfNoRoots: ->
    @setupDropzone() if @collection.length is 0

  _startDrag: ->
    # store current sly scroll as offset
    @slyStartOffset = @sly.pos.cur

  # scroll sly while dragging
  _drag: (v, e, ui) ->
    y      = ui.offset.top - @$frame.offset().top
    frame  = @sly.rel.frameSize
    zone   = frame * 0.2
    speed  = 1000

    if y < zone
      # if in the top quarter of frame slide up
      @sly.moveBy -speed * (zone - y) / zone
    else if y > frame - zone
      # if in the bottom quarter of frame slide down
      @sly.moveBy speed * (zone - (frame - y)) / zone
    else
      @sly.stop()

  _drop: (receiver, e, ui) ->
    placement = 'after'  if $(e.target).hasClass 'below'
    placement = 'before' if $(e.target).hasClass 'above'
    placement = 'into'   if $(e.target).hasClass 'into'
    placement = 'root'   if $(e.target).attr('id') is 'tree'
    target    = ui.draggable.data 'view'
    type      = if target.$el.hasClass('page') then 'page' else 'link'

    if receiver.model?
      # uniqueness, don't insert if page is already linked at receiver
      return if receiver.model.validateMove target.model, placement, type
      return if target.model.get('slug') == receiver.model.get('slug')
    # if page create new link model and link branch
    if type is 'page'
      # create model attached to end of roots so moving it will not disrupt existing nav
      model = new Link
        slug: target.model.get('slug')
        title: target.model.get('title')
        page_id: target.model.id
        ancestry: ''
        position: @collection.length
      @collection.add model
      target = @createBranch model

    switch placement
      when 'after'
        receiver.$el.after target.$el
      when 'before'
        receiver.$el.before target.$el
      when 'into'
        receiver.$children.append target.$el
      when 'root'
        @insertRoot target

    if placement is 'root'
      target.model.save()
    else
      target.model.insert receiver.model, placement
