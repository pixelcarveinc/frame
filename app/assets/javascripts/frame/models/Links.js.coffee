#= require frame/namespace
#= require frame/models/Link


class window.chocolate.frame.models.Links extends Backbone.Collection
  
  model: chocolate.frame.models.Link
        
  comparator: 'position'
  
  initialize: ->
    @listenTo @, 'destroy', @_destroy
  
  url: JST.frameLinksPath
  
  set: ->
    ret = super
    m.parent = @parent for m in @models
    ret

  # Make space for changing old position to new by shifting.
  # 
  # @param {int} oldPosition
  # @param {int} newPosition
  # @returns {this}
  move: (oldPosition, newPosition) ->
    oldPosition = parseInt oldPosition
    newPosition = parseInt newPosition
    
    for m in @models
      p = parseInt m.get('position')
      if newPosition < oldPosition
        # moving up
        if p >= newPosition and p < oldPosition
          m.set 'position', p + 1
      else
        # moving down
        if p > oldPosition and p <= newPosition
          m.set 'position', p - 1
      
    @sort()
    @
    
  # Shift all positions from a specified position by amount.
  shift: (from, amount) ->
    from   = parseInt from
    amount = parseInt amount
    for m in @models
      p = parseInt m.get('position')
      m.set('position', p + amount) if p >= from
      
  # For testing position, returns a simplified representation of page tree.
  # 
  # @returns {Object}
  debugPosition: ->
    reducer = (memo, model) ->
      obj = title: model.get('title')
      if model.get('children').length
        obj['children'] = model.get('children').debugPosition()
      memo[model.get('position')] = obj
      memo
      
    @reduce reducer, {}
    
  _destroy: (model) ->
    @shift parseInt(model.get('position')), -1