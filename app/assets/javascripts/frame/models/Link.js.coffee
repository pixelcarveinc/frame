#= require frame/namespace
#= require pxcv/mixins/Associations

class window.chocolate.frame.models.Link extends Backbone.Model
  
  associations: ->
    children:
      build:  chocolate.frame.models.Links
      type:   'collection'
      create: (c) ->
        c.parent = @
        m.parent = @ for m in c.models
        
  # Validate that model can be inserted at placement. 
  validateMove: (model, placement, type) ->
    c = if placement is 'into' then @get('children') else @collection
    
    page_id = if type is 'page' then model.id else model.get('page_id')
    
    # for each model in collection that isn't equal, if page_id is equal then return true (invalid)
    (m for m in c.models when m isnt model and m.get('page_id') is page_id).length > 0
  
  # Insert this model at receiver.
  insert: (receiver, placement) ->
    if placement is 'into'
      @insertAsChild receiver
    else
      if @id? and receiver.parent is @parent
        # treat as order change if parents match and model isn't new
        @changeOrder receiver, placement is 'after'
      else
        @insertAsSibling receiver, placement is 'after'
  
  changeOrder: (receiver, after) ->
    # order change
    oldP = parseInt @get('position')
    newP = parseInt receiver.get('position')
    newP -= 1 if not after and oldP < newP
    newP += 1 if after and newP < @get('children').length - 1
    
    @collection.move oldP, newP
    @save position: newP
    
  # Change parent to receiver's parent and update positions.
  insertAsSibling: (receiver, after) ->
    oldC = @collection
    newC = receiver.collection
    oldP = parseInt @get('position')
    newP = parseInt receiver.get('position')
    newP += 1 if after
    
    # remove from this collection
    oldC.remove @
    # shift model after this one in old collection by -1
    oldC.shift oldP + 1, -1
    # shift model after this one in new collection by 1
    newC.shift newP, 1
    # save position and parent_id
    parent_id = if newC.parent? then newC.parent.id else ''
    # add to receiver collection
    newC.add @, at: newP
    @save position: newP, parent_id: parent_id
    
  # Change parent to receiver, append to children and update positions.
  insertAsChild: (receiver) ->
    oldC = @collection
    newC = receiver.get('children')
    oldP = parseInt @get('position')
    newP = newC.length
    
    # remove from this collection
    oldC.remove @
    # shift model after this one in old collection by -1
    oldC.shift oldP + 1, -1
    # append to receiver's children
    newC.add @
    # save position and parent_id
    @save position: newC.length - 1, parent_id: receiver.id
    
  
pxcv.mixins.Associations.mixin chocolate.frame.models, 'Link'
