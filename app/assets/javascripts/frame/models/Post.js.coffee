#= require frame/namespace


class window.chocolate.frame.models.Post extends Backbone.Model

  url: -> JST.frameBlogPostPath(@get('blog_id'), @id)
