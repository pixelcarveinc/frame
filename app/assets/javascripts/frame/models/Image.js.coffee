#= require frame/namespace


class window.chocolate.frame.models.Image extends Backbone.Model

  url: -> JST.frameGalleryImagePath(@get('gallery_id'), @id)
