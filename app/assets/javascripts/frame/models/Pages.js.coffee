#= require frame/namespace
#= require frame/models/Link


class window.chocolate.frame.models.Pages extends Backbone.Collection
  
  model: chocolate.frame.models.Page
  
  url: JST.framePagesPath
  