module Frame
  class ExternalRoutable < Routable
    
    def page_routes(mapper)
      # noop, this page doesn't have routes
    end
     
  end
end