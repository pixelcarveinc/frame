module Frame
  class GalleryRoutable < Routable
    
    def content_routes(mapper)
      mapper.link.page.images.where(published: true).each do |image|
        mapper.get image.slug, {
          controller: Frame::ImagesController,
          defaults: {link_id: mapper.link.id, image_id: image.id}
        }
      end
    end
    
  end
end