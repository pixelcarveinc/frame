module Frame
  class ContactRoutable < Routable
    
    def page_routes(mapper)
      mapper.get action: :new, controller: Frame::MessagesController
      mapper.post action: :create, controller: Frame::MessagesController
    end 
  end
end