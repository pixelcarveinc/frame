module Frame
  class BlogRoutable < Routable
    
    def content_routes(mapper)
      mapper.link.page.posts.where(published: true).each do |post|
        mapper.get post.slug, {
          controller: Frame::PostsController,
          defaults: {link_id: mapper.link.id, post_id: post.id},
        }
      end
    end
    
  end
end