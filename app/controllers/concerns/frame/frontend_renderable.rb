module Frame
  module FrontendRenderable
    extend ActiveSupport::Concern
    
    included do
      skip_authorization_check only: :show
      skip_before_action :authenticate!, only: :show
      
      register_frontend_action :show
    
      before_action :set_link, only: :show
      
      decorates_assigned :link, :page
      decorates_assigned :links, with: LinkTreeDecorator
      decorates_assigned :pages, with: PagesDecorator
    end
  
    private
      # Set all links as ordered hash
      def set_link
        @link = Link.find params[:link_id]
        @page = @link.page
        @links = Link.arrange
      end
  end
end