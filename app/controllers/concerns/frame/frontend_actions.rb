module Frame
  module FrontendActions
    extend ActiveSupport::Concern

    def is_frontend?(controller, action = nil)
      self.class.is_frontend? controller, action
    end

    module ClassMethods
      def register_frontend_action(controller, action = nil)
        if action.nil?
          action = controller
          controller = self
        end

        registered_frontend_actions(controller) << action.to_sym
      end

      def unregister_frontend_action(controller, action = nil)
        if action.nil?
          action = controller
          controller = self
        end

        registered_frontend_actions(controller).delete_if { |a| a == action.to_sym }
      end

      def is_frontend?(controller, action = nil)
        if action.nil?
          action = controller
          controller = self
        end

        registered_frontend_actions(controller).include? action.to_sym
      end

      private

        def registered_frontend_actions(controller)
          @@registered_frontend_actions ||= {}
          @@registered_frontend_actions[controller] ||= []
        end

    end
  end
end
