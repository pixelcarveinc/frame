require_dependency "frame/application_controller"

module Frame
  class PagesController < ApplicationController
    include FrontendRenderable

    load_and_authorize_resource except: [:show, :create, :multi_destroy], class: Frame::Page

    add_breadcrumb 'pages', :links_path
    add_breadcrumb 'new',   :new_page_path,   only: :new
    add_breadcrumb 'edit',  :edit_page_path,  only: :edit

    # GET/pages/show
    def show
      #debugger
      respond_to do |format|
        format.html { render template: page_template(@page) }
        format.json { render json: @page.as_json(link: @link) }
        format.any { render template: page_template(@page) }
      end
    end

    # GET /pages/new
    def new
      # redirect to type specific controller if set and path helper exists
      #debugger

      unless params['type'].blank?

        helper = "new_#{params['type'].demodulize.underscore}_path"
        redirect_to send(helper, @page.update_attributes(type: params['type'])) if respond_to?(helper)
      end
    end

    # GET /pages/1/edit
    def edit
      # redirect to type specific controller if set and path helper exists
      unless page.type.blank?
        helper = "edit_#{page.type.demodulize.underscore}_path"
        redirect_to send(helper, @page) if respond_to?(helper)
      end
    end

    # POST /pages
    def create
      @page = Page.new(page_params)
      authorize! :create, @page

      respond_to do |format|
        if @page.save
          format.html { redirect_to edit_page_path(@page.id), notice: 'Page was successfully created.' }
          format.json { render json: @page, status: :created, location: @page }
        else
          format.html { render action: 'new' }
          format.json { render json: @page.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /pages/1
    def update
      respond_to do |format|
        if @page.update(page_params)
          format.html { redirect_to links_path, notice: 'Page was successfully updated.' }
          format.json { render json: @page, status: :ok, location: @page }
        else
          format.html { render action: 'edit' }
          format.json { render json: @page.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /pages/1
    def destroy
      @page.destroy
      respond_to do |format|
        format.html { redirect_to links_url, notice: 'Page was successfully deleted.' }
        format.json { head :no_content }
      end
    end

    # DELETE /pages
    # DELETE /pages.json
    def multi_destroy
      @pages = Page.find params[:id]
      authorize! :destroy, @pages

      Page.destroy @pages

      respond_to do |format|
        format.html { redirect_to pages_url }
        format.json { head :no_content }
      end
    end

    private
      # Only allow a trusted parameter "white list" through.
      def page_params
        params.require(:page).permit(:type, :title, :body, :meta_description, :head_title)
      end

      # Allows lightweight page types that do not have a controller to specify
      # a custom show template.
      def page_template(page)
        if page.type
          template = "frame/#{page.type.demodulize.pluralize.underscore}/show"
          template_exists?(template) ? template : 'frame/pages/show'
        else
          'frame/pages/show'
        end
      end
  end
end
