require_dependency "frame/application_controller"

module Frame
  class ExternalsController < ApplicationController
    load_and_authorize_resource class: Frame::External
    
    add_breadcrumb 'pages', :links_path
    add_breadcrumb 'edit',  :edit_external_path,  only: :edit
    
    decorates_assigned :external
    

    # GET /externals/1/edit
    def edit
    end

    # PATCH/PUT /externals/1
    def update
      respond_to do |format|
        if @external.update(external_params)
          format.html { redirect_to links_path, notice: 'Page was successfully updated.' }
          format.json { render json: @external, status: :ok, location: @external }
        else
          format.html { render action: 'edit' }
          format.json { render json: @external.errors, status: :unprocessable_entity }
        end
      end
    end

    
    private
      # Only allow a trusted parameter "white list" through.
      def external_params
        params.require(:external).permit(:type, :title, :link_url, :link_target)
      end
  end
end
