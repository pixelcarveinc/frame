require_dependency "frame/application_controller"

module Frame
  class ImagesController < ApplicationController
    include FrontendRenderable
    
    before_action :set_gallery, except: :show
    before_action :set_image, only: :show
    
    load_and_authorize_resource except: [:show, :create, :multi_destroy], through: :gallery, class: Frame::Image
    
    decorates_assigned :gallery, :image, :images

    # GET /galleries/1/images
    def index
    end

    # GET /galleries/1/images/1
    def show
    end

    # GET /galleries/1/images/new
    def new
      add_breadcrumb 'new', :new_gallery_image_path
    end

    # GET /galleries/1/images/1/edit
    def edit
      add_breadcrumb 'edit', :edit_gallery_image_path
    end

    # POST /galleries/1/images
    def create
      @image = @gallery.images.new(image_params)
      authorize! :create, @image
      
      respond_to do |format|
        if @image.save
          format.html { redirect_to gallery_images_path(@gallery), notice: 'Image was successfully created.' }
          format.json { render json: @image, status: :created, location: [@gallery, @image] }
        else
          format.html { render action: 'new' }
          format.json { render json: @image.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /galleries/1/images/1
    def update
      respond_to do |format|
        if @image.update(image_params)
          format.html { redirect_to gallery_images_path(@gallery), notice: 'Image was successfully updated.' }
          format.json { render json: @image, status: :ok, location: [@gallery, @image] }
        else
          format.html { render action: 'edit' }
          format.json { render json: @image.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /galleries/1/images/1
    def destroy
      @image.destroy
      respond_to do |format|
        format.html { redirect_to gallery_images_url(@gallery), notice: 'Image was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  
    # DELETE /blogs/1/posts
    def multi_destroy
      @images = Image.find params[:id]
      authorize! :destroy, @images
      
      Image.destroy @images
      
      respond_to do |format|
        format.html { redirect_to gallery_images_url(@gallery) }
        format.json { head :no_content }
      end
    end

    private
      def set_gallery
        @gallery = Gallery.find(params[:gallery_id])
        add_breadcrumb 'pages', :links_path
        add_breadcrumb @gallery.title, edit_page_path(@gallery)
        add_breadcrumb 'images', :gallery_images_path
      end
      
      def set_image
        @gallery = @page
        @image = @gallery.images.find(params[:image_id])
      end

      # Only allow a trusted parameter "white list" through.
      def image_params
        params.require(:image).permit(:title, :alt, :body, :published, :position, :image)
      end
  end
end
