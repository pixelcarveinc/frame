require_dependency "frame/application_controller"

module Frame
  class MessagesController < ApplicationController
    include FrontendRenderable

    unregister_frontend_action :show
    register_frontend_action :new
    register_frontend_action :create

    skip_authorization_check only: [:new, :create]
    skip_before_action :authenticate!, only: [:new, :create]

    load_and_authorize_resource except: [:new, :create, :multi_destroy], class: Frame::Message

    add_breadcrumb 'pages', :links_path

    before_action :set_contact, except: [:new, :create]
    before_action :set_link, :set_message, only: [:new, :create]

    decorates_assigned :contact, :message, :messages

    # GET /contacts/1/frame/messages
    def index
      @messages = @messages.order(created_at: :desc)
    end

    # GET /contacts/1/frame/messages/1
    def show
      add_breadcrumb 'show', :contact_message_path
    end

    # GET /contacts/1/frame/messages/new
    def new
      @message = @contact.messages.new
    end

    # POST /contacts/1/frame/messages
    def create
      @message = @contact.messages.new(message_params)

      respond_to do |format|
        if @message.save
          MessageMailer.message_email(@message).deliver
          format.any { redirect_to main_app.link_slug_path(@link), notice: 'Thank you, your message has been received.' }
          format.json { render json: @message, status: :created, location: [@contact, @message] }
        else
          format.any { render action: 'new' }
          format.json { render json: @message.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /contacts/1/frame/messages/1
    def destroy
      @message.destroy
      respond_to do |format|
        format.html { redirect_to contact_messages_url(@contact) }
        format.json { head :no_content }
      end
    end

    # DELETE /contacts/1/messages
    def multi_destroy
      @messages = Message.find params[:id]
      authorize! :destroy, @messages

      Message.destroy @messages

      respond_to do |format|
        format.html { redirect_to contact_messages_url(@contact) }
        format.json { head :no_content }
      end
    end

    private
      def set_contact
        @contact = Contact.find(params[:contact_id])
        add_breadcrumb 'pages', :links_path
        add_breadcrumb @contact.title, edit_page_path(@contact)
        add_breadcrumb 'messages', :contact_messages_path
      end

      def set_message
        @contact = @page
      end

      # Never trust parameters from the scary internet, only allow the white list through.
      def message_params
        params.require(:message).permit(:name, :email_address, :comments)
      end
  end
end
