require_dependency "frame/application_controller"

module Frame
  class BlogsController < ApplicationController
    include FrontendRenderable
    
    load_and_authorize_resource except: :show, class: Frame::Blog
    
    before_action :set_blog, :set_posts, only: :show
    
    add_breadcrumb 'pages', :links_path
    add_breadcrumb 'edit',  :edit_blog_path,  only: :edit
  
    decorates_assigned :blog, :posts
    
    # GET /blogs
    def index
    end

    # GET /blogs/1
    def show
    end

    # GET /blogs/1/edit
    def edit
    end

    # PATCH/PUT /blogs/1
    def update
      respond_to do |format|
        if @blog.update(blog_params)
          format.html { redirect_to links_path, notice: 'Page was successfully updated.' }
          format.json { render json: @blog, status: :ok, location: @blog }
        else
          format.html { render action: 'edit' }
          format.json { render json: @blog.errors, status: :unprocessable_entity }
        end
      end
    end

    private
      def set_blog
        @blog = @page
      end
      
      def set_posts
        @posts = @blog.posts.where(published: true).order(publish_date: :desc)
      end
      
      # Only allow a trusted parameter "white list" through.
      def blog_params
        params.require(:blog).permit(:type, :title, :body, :meta_description, :head_title)
      end
  end
end
