require_dependency "frame/application_controller"

module Frame
  class GalleriesController < ApplicationController
    include FrontendRenderable
    
    load_and_authorize_resource except: :show, class: Frame::Gallery
    
    before_action :set_gallery, :set_images, only: :show
    
    add_breadcrumb 'pages', :links_path
    add_breadcrumb 'edit',  :edit_blog_path,  only: :edit
  
    decorates_assigned :gallery, :images

    # GET /galleries
    def index
    end

    # GET /galleries/1
    def show
    end

    # GET /galleries/1/edit
    def edit
    end

    # PATCH/PUT /galleries/1
    def update
      respond_to do |format|
        if @gallery.update(gallery_params)
          format.html { redirect_to links_path, notice: 'Page was successfully updated.' }
          format.json { render json: @gallery, status: :ok, location: @gallery }
        else
          format.html { render action: 'edit' }
          format.json { render json: @gallery.errors, status: :unprocessable_entity }
        end
      end
    end

    private
      def set_gallery
        @gallery = @page
      end
      
      def set_images
        @images = @gallery.images.where(published: true).order(:position)
      end

      # Only allow a trusted parameter "white list" through.
      def gallery_params
        params.require(:gallery).permit(:type, :title, :body, :meta_description, :head_title)
      end
  end
end
