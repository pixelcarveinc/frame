module Frame
  class ApplicationController < ::ApplicationController
    include ::PublicActivity::StoreController
    include FrontendActions

    check_authorization
    before_action :authenticate!

    # before_action :set_locale

    # def set_locale
    #   if request.subdomain == 'fr' || request.subdomain.include?("fr.")
    #     I18n.locale = :fr

    #   else
    #     I18n.locale = :en

    #   end
    # end
  end
end
