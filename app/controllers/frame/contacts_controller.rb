require_dependency "frame/application_controller"

module Frame
  class ContactsController < ApplicationController
    include FrontendRenderable
    
    load_and_authorize_resource except: :show, class: Frame::Contact
    
    add_breadcrumb 'pages', :links_path
    add_breadcrumb 'edit',  :edit_contact_path,  only: :edit
  
    decorates_assigned :contact

    # GET /contacts/1/edit
    def edit
    end

    # PATCH/PUT /contacts/1
    def update
      respond_to do |format|
        if @contact.update(contact_params)
          format.html { redirect_to links_path, notice: 'Page was successfully updated.' }
          format.json { render json: @contact, status: :ok, location: @contact }
        else
          format.html { render action: 'edit' }
          format.json { render json: @contact.errors, status: :unprocessable_entity }
        end
      end
    end

    private
      # Only allow a trusted parameter "white list" through.
      def contact_params
        params.require(:contact).permit(:type, :title, :body, :meta_description, :head_title, :emails)
      end
    
    
  end
end
