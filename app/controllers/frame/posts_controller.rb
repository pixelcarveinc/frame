require_dependency "frame/application_controller"

module Frame
  class PostsController < ApplicationController
    include FrontendRenderable
    
    before_action :set_blog, except: :show
    before_action :set_post, only: :show
    
    load_and_authorize_resource except: [:show, :create, :multi_destroy], through: :blog, class: Frame::Post
    
    decorates_assigned :blog, :post, :posts

    # GET /blogs/1/posts
    def index
    end

    # GET /blogs/1/posts/1
    def show
    end

    # GET /blogs/1/posts/new
    def new
      add_breadcrumb 'new', :new_blog_post_path
    end

    # GET /blogs/1/posts/1/edit
    def edit
      add_breadcrumb 'edit', :edit_blog_post_path
    end

    # POST /blogs/1/posts
    def create
      @post = @blog.posts.new(post_params)
      authorize! :create, @post
      
      respond_to do |format|
        if @post.save
          format.html { redirect_to blog_posts_path(@blog), notice: 'Post was successfully created.' }
          format.json { render json: @post, status: :created, location: [@blog, @post] }
        else
          format.html { render action: 'new' }
          format.json { render json: @post.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /blogs/1/posts/1
    def update
      respond_to do |format|
        if @post.update(post_params)
          format.html { redirect_to blog_posts_path(@blog), notice: 'Post was successfully updated.' }
          format.json { render json: @post, status: :ok, location: [@blog, @post] }
        else
          format.html { render action: 'edit' }
          format.json { render json: @post.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /blogs/1/posts/1
    def destroy
      @post.destroy
      respond_to do |format|
        format.html { redirect_to blog_posts_url(@blog), notice: 'Post was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  
    # DELETE /blogs/1/posts
    def multi_destroy
      @posts = Post.find params[:id]
      authorize! :destroy, @posts
      
      Post.destroy @posts
      
      respond_to do |format|
        format.html { redirect_to blog_posts_url(@blog) }
        format.json { head :no_content }
      end
    end


    private
      def set_blog
        @blog = Blog.find(params[:blog_id])
        add_breadcrumb 'pages', :links_path
        add_breadcrumb @blog.title, edit_page_path(@blog)
        add_breadcrumb 'posts', :blog_posts_path
      end
      
      def set_post
        @blog = @page
        @post = @blog.posts.find params[:post_id]
      end

      # Only allow a trusted parameter "white list" through.
      def post_params
        params.require(:post).permit(:blog_id, :category, :title, :body, :publish_date, :published)
      end
  end
end
