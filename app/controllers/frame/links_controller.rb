require_dependency "frame/application_controller"

module Frame
  class LinksController < ApplicationController
    load_and_authorize_resource except: [:index, :create, :multi_destroy], class: Frame::Link
    
    wrap_parameters Link, include: [:position, :page_id, :slug, :created_at, :updated_at, :parent_id]
    
    add_breadcrumb 'pages', :links_path
    add_breadcrumb 'show',  :link_path,       only: :show
    add_breadcrumb 'new',   :new_link_path,   only: :new
    add_breadcrumb 'edit',  :edit_link_path,  only: :edit

    decorates_assigned :link
    decorates_assigned :links, with: LinkTreeDecorator
    decorates_assigned :pages, with: PagesDecorator

    # GET /links
    def index
      authorize! :read, Link
      authorize! :read, Page
      
      @links = Link.arrange
      @pages = Page.order(:title)
    end

    # GET /links/new
    def new
    end

    # GET /links/1/edit
    def edit
    end

    # POST /links
    def create
      authorize! :create, @link


      respond_to do |format|

        if params[:parent_id].present? && Frame::Link.find(params[:parent_id]).slug == params[:slug]
          format.json { render json: {errors: "Can't be same type as parent"}, status: :unprocessable_entity }
          
        else
          
          @link = Link.new(link_params)
          


        
          if @link.save
            format.html { redirect_to @link, notice: 'Link was successfully created.' }
            format.json { render json: @link, status: :created, location: @link }
          else
            format.html { render action: 'new' }
            format.json { render json: @link.errors, status: :unprocessable_entity }
          end
        end
      end
    end

    # PATCH/PUT /links/1
    def update
      respond_to do |format|
        if @link.update(link_params)
          format.html { redirect_to @link, notice: 'Link was successfully updated.' }
          format.json { render json: @link, status: :ok, location: @link }
        else
          format.html { render action: 'edit' }
          format.json { render json: @link.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /links/1
    def destroy
      @link.destroy
      respond_to do |format|
        format.html { redirect_to links_url, notice: 'Link was successfully destroyed.' }
        format.json { head :no_content }
      end
    end
  
    # DELETE /pages
    # DELETE /pages.json
    def multi_destroy
      # TODO implement me!
    end

    private
      # Only allow a trusted parameter "white list" through.
      def link_params
        params.require(:link).permit(:parent_id, :position, :page_id, :slug)
      end
  end
end
