module Frame
  class ImageDecorator < Draper::Decorator
    delegate_all

    def path(options = {})
      if object.new_record?
        h.gallery_images_path object.gallery, options
      else
        h.gallery_image_path object.gallery, object, options
      end
    end

    def edit_path(options = {})
      h.edit_gallery_image_path object.gallery, object, options
    end
    
    def row_json
      object.to_json only: [:id, :gallery_id, :position, :published]
    end
      
    def lightbox_thumb
      h.link_to object.image.url, title: object.title, data: { lightbox: 'thumb' } do
        h.image_tag object.image.url(:tiny)
      end
    end
  
  end
end
