module Frame
  class PagesDecorator < Draper::CollectionDecorator

    def to_json(options={})
      object.collect do |page|
        page_as_json(page, options)
      end.to_json
    end
  
    private
    
      def page_as_json(page, options={})
        json = page.as_json options
        target = options[:root] ? json['post'] : json
        exclude = [:created_at, :updated_at, :body, :publish_date]
        
        target.delete_if { |k| exclude.include? k.to_sym }
        target[:slug] = page.title.create_id
        json
      end
      
  end
end