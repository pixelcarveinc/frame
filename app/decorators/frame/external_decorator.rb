module Frame
  class ExternalDecorator < PageDecorator
    def link_targets
      External::LINK_TARGETS
    end
  end
end
