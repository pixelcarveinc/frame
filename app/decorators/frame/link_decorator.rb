module Frame
  class LinkDecorator < Draper::Decorator
    delegate_all
    
    decorates_association :page
    decorates_association :parent
    decorates_association :root
    decorates_association :ancestors
    decorates_association :children
    decorates_association :siblings
    decorates_association :descendants
    decorates_association :subtree
    decorates_association :higher_item
    decorates_association :higher_items
    decorates_association :lower_item
    decorates_association :lower_items
  
    def slug_path(options = {})
      h.main_app.link_slug_path(object, options)
    end
  
    def slug_url(options = {})
      h.main_app.link_slug_url(object, options)
    end
    
    def path(options = {})
      object.new_record? ? h.links_path(options) : h.link_path(object, options) 
    end
    
    def edit_path(options = {})
      h.edit_link_path(object, options)
    end
  
    # Strip out some unused attributes when converting to JSON for JavaScript 
    def as_json(options = {})
      json = object.as_json options
      target = options[:root] ? json['link'] : json
      target.delete 'updated_at'
      target.delete 'created_at'
      target.delete 'ancestry_depth'
      target['title'] = page.title
      
      json
    end
  
    private
      # Extend link slug path helper to return slug paths.
      def link_slug_path(link, options = {})
        link = Frame.routing_table(I18n.locale).slugs[link] if link.is_a? Frame::Link

        
        if link.blank?
          h.main_app.link_root_path options
        else
          h.main_app.link_slug_path link, options
        end
      end
      
      # Extend link slug url helper to return slug urls.
      def link_slug_url(link, options = {})
        link = Frame.routing_table(I18n.locale).slugs[link] if link.is_a? Frame::Link
        if link.blank?
          h.main_app.link_root_url options
        else
          h.main_app.link_slug_url link, options
        end
      end
  
  end
end