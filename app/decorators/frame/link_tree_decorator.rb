module Frame
  
  # Decorates ancestry arranged link tree.
  class LinkTreeDecorator < Draper::CollectionDecorator
    include Frame::Helpers::MenuHelper
    include Frame::Helpers::RoutingTableHelper
    
    def to_json

      decorated_collection.keys.collect do |link|

        link_as_json(link, decorated_collection[link])
      end.to_json
    end
    
    # Overridden for custom decoration of arranged hash.
    def decorated_collection

      @decorated_collection ||= object.keys.reduce({}) do |memo, link|

        decorate_hash memo, link, object[link]
      end


      
    end

    
    private
    
      def decorate_hash(memo, link, children)

        memo[decorate_item(link)] = children.keys.reduce({}) do |m, l|
          decorate_hash(m, l, children[l])
        end
        memo 
      end
    
      def link_as_json(link, children)

        json = link.as_json
        json['children'] = children.keys.map do |child|
          link_as_json(child, children[child])
        end
        json 
      end
    
  end
end