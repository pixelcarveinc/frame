module Frame
  class MessageDecorator < Draper::Decorator
    delegate_all

    def path(options = {})
      if object.new_record?
        h.contact_messages_path object.contact, options
      else
        h.contact_message_path object.contact, object, options
      end
    end
    
    def comments_short
      truncate object.comments
    end
    
    private
      
      def truncate(str, options = {})
        options = {words: 12}.merge options
        w = str.split(/\s/)
        n = options[:words]
        w[0...n].join(' ') + (w.size > n ? '...' : '')
      end
  end
end
