module Frame
  class GalleryDecorator < PageDecorator
    decorates_association :images

    def images_path(options = {})
      h.gallery_images_path object, options
    end

    def new_image_path(options = {})
      h.new_gallery_image_path object, options
    end
  end
end
