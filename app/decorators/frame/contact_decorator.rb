module Frame
  class ContactDecorator < PageDecorator
    def messages_path
      h.contact_messages_path object
    end
    
  end
end
