module Frame
  class BlogDecorator < PageDecorator
    decorates_association :posts
    
    def posts_path(options = {})
      h.blog_posts_path object, options
    end
    
    def new_post_path(options = {})
      h.new_blog_post_path object, options
    end
  end
end