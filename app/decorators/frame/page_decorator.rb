module Frame
  class PageDecorator < Draper::Decorator
    delegate_all

    # Output head title tag from page title or page head title if it exists.
    #
    # options:
    # prepend     - prepend string to title
    # append      - append string to title
    # separator   - separator between append/prepend and title, default is ' | '
    def head_title_tag(options = {})
      options.symbolize_keys!

      separator = options[:separator] || ' | '
      content = object.head_title.blank? ? object.title : object.head_title
      content += separator + options[:append] if options[:append]
      content = options[:prepend] + separator + content if options[:prepend]

      h.content_tag :title, content
    end
    def link_to_toggle
      h.link_to '', '#', { class: '', title: "Toggle #{object.title} page" }
    end
    def meta_description_tag
      unless object.meta_description.blank?
        h.tag :meta, name: 'description', content: object.meta_description
      end
    end

    def path(options = {})
      object.new_record? ? h.pages_path(options) : h.page_path(object, options)
    end

    def edit_path(options = {})
      h.edit_page_path(object, options)
    end

    def link_to_edit_icon
      h.link_to '', edit_path, { class: 'icon-edit', title: "Edit #{object.title} page" }
    end

    def link_to_delete_icon
      h.link_to '', path, { class: 'icon-delete', title: "Delete #{object.title} page",
        method: :delete, data: {confirm: "Are you sure you wish to delete <b>#{object.title}</b>?<br/>It cannot be recovered and all links to this page will be deleted as well."}
      }
    end

    def select_types
      types = Page.types.sort.collect { |t| [t.demodulize.underscore.humanize, t] }
      types.unshift ['Default', '']
    end

  end
end
