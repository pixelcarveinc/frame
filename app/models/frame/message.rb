module Frame
  class Message < ActiveRecord::Base
    belongs_to :contact, inverse_of: :messages
    
    validates :contact_id, :name, presence: true
    validates :email_address, presence: true, email: true
    validates :comments, presence: true, plain_text: true
  end
end
