module Frame
  class Link < ActiveRecord::Base
    include ::PublicActivity::Model
    include GeneratesRoutes
    
    tracked params: {title: proc { |c, m| m.page.title }}, owner: proc { |c, m| c.current_user if c }
    
    has_ancestry cache_depth: true
    acts_as_list scope: ['ancestry'], top_of_list: 0
    
    belongs_to :page, inverse_of: :links
    
    validates :page_id, presence: true
    validates :page_id, uniqueness: { scope: :ancestry }
    
    before_save :prepare_slug
    after_save :reload_routes
    after_destroy :reload_routes
    
    
    class << self
      # alias class method for override
      alias_method :_arrange, :arrange
    end
    
    # Customized arrange, defaults order to position, includes associated page.
    def self.arrange(options = {})
      options = {order: :position}.merge options
      
      eager_load(:page)._arrange options
    end
    
    
    private
    
      def prepare_slug
        # generate a default slug from page title
        self.slug = page.title.create_id if slug.blank?
        # create unique slug if one already exists at ancestry
        self.slug = unique_slug slug
      end
      
      def unique_slug(slug)
        if siblings.any? { |s| s != self && s.slug == slug }
          # duplicate slug, need to make it unique
          # if -\d+$ matches increment end number, otherwise add a -1
          if slug =~ /-(\d+)$/
            slug.sub!(/\d+$/, ($~[1].to_i + 1).to_s)
          else
            slug = slug + '-1'
          end
          
          # try again
          return unique_slug slug
        end
        slug
      end
  end
end
