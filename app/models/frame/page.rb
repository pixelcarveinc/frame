module Frame
  class Page < ActiveRecord::Base
    include ::PublicActivity::Model

    
    tracked params: {title: :title}, owner: proc { |c, m| c.current_user if c }
    
    has_many :links, inverse_of: :page, dependent: :destroy
    validates :title, presence: true
    
    after_save :update_links
    
    
    def self.types
      @types ||= Rails.application.config.frame.page_types
      @types.clone
    end
  
    def as_json(options = {})
      json = super options
      target = options[:root] ? json['page'] : json
      target['slug'] = options[:link].slug if options[:link]
      target['type'] = type.blank? ? 'Default' : type.demodulize
      json
    end
    
    
    private
    
      def update_links

        

        
        links.each { |l| l.update slug: nil } if title_changed?

        Frame.routing_table(I18n.locale).reload!
      end
  end
end
