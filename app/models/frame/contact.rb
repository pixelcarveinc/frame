module Frame
  class Contact < Page
    has_many :messages, inverse_of: :contact, dependent: :destroy
    
    validates :emails, presence: true
  end
end
