module Frame
  class External < Page
    LINK_TARGETS = %w(_blank _self _top _parent)
    
    validates :link_url, presence: true#, url: true
    validates :link_target, inclusion: {in: LINK_TARGETS, message: "%{value} is not a valid link target"}
    
    before_validation :set_defaults
    
    private
      def set_defaults
        self.link_target = '_blank' if link_target.blank?
      end
    
  end
end
