module Frame
  class Post < ActiveRecord::Base
    include ::PublicActivity::Model
    include GeneratesRoutes
    
    tracked params: {title: :title}, owner: proc { |c, m| c.current_user if c }
    
    belongs_to :blog, inverse_of: :posts
    
    validates :title, presence: true
    validates_presence_of :blog
    
    after_initialize :set_default_publish_date
    before_save :prepare_slug
    
    # TODO dry up slug stuff in a concern
      
    private
      def set_default_publish_date
        self.publish_date = Time.now.to_s(:na) if publish_date.nil?
      end
    
      def prepare_slug
        self.slug = title.create_id if slug.blank?
        self.slug = unique_slug slug
      end
      
      def unique_slug(slug)
        if blog.posts.any? { |p| p != self && p.slug == slug }
          # duplicate slug, need to make it unique
          # if -\d+$ matches increment end number, otherwise add a -1
          if slug =~ /-(\d+)$/
            slug.sub!(/\d+$/, ($~[1].to_i + 1).to_s)
          else
            slug = slug + '-1'
          end
          
          # try again
          return unique_slug slug
        end
        slug
      end
  end
end
