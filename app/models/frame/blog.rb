module Frame
  class Blog < Page
    has_many :posts, inverse_of: :blog, dependent: :destroy
    accepts_nested_attributes_for :posts, allow_destroy: true
  end
end
