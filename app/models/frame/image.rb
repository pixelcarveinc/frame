module Frame
  class Image < ActiveRecord::Base
    include ::PublicActivity::Model
    include GeneratesRoutes
    include HasImage

    tracked params: {title: :title}, owner: proc { |c, m| c.current_user if c }

    belongs_to :gallery, inverse_of: :images
    acts_as_list scope: :gallery, top_of_list: 0

    validates_presence_of :gallery

    before_save :set_title
    before_save :set_alt
    before_save :prepare_slug

    private

      def set_title
        if title.blank?
          self.title = ::File.basename image_file_name, ::File.extname(image_file_name)
        end
      end

      def set_alt
        self.alt = '' if alt.blank?
      end

      def prepare_slug
        self.slug = title.create_id if slug.blank?
        self.slug = unique_slug slug
      end

      def unique_slug(slug)
        if gallery.images.any? { |p| p != self && p.slug == slug }
          # duplicate slug, need to make it unique
          # if -\d+$ matches increment end number, otherwise add a -1
          if slug =~ /-(\d+)$/
            slug.sub!(/\d+$/, ($~[1].to_i + 1).to_s)
          else
            slug = slug + '-1'
          end

          # try again
          return unique_slug slug
        end
        slug
      end
  end
end
