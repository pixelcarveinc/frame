module Frame

  # Include in models that have an attached image, override in project to
  # change styles and processors.
  module HasImage
    extend ActiveSupport::Concern

    included do
      has_attached_file :image,
        styles: {
          hires:  ['1200x>',      :jpg],
          midres:  ['768x>',      :jpg],
          lores:  ['550x>',      :jpg],
          tiny:   ['40x40#',     :jpg],
          small:  ['100x>',      :jpg],
          medium: ['250x250>',   :jpg],
          large:  ['500x500>',   :jpg],
          huge:   ['1000x1000>', :jpg],
          slideshow:   ['1920x500#', :jpg] },
        convert_options: {
          hires:   '-blur 0x10 -fill black -colorize 75% -strip -depth 8 -quality 90',
              midres:   '-blur 0x10 -fill black -colorize 75% -strip -depth 8 -quality 90',
              lores:   '-blur 0x10 -fill black -colorize 75% -strip -depth 8 -quality 90',
          tiny:   '-colorspace RGB -flatten -strip -depth 8 -quality 90',
          small:  '-colorspace RGB -flatten -strip -depth 8 -quality 90',
          medium: '-colorspace RGB -flatten -strip -depth 8 -quality 90',
          large:  '-colorspace RGB -flatten -strip -depth 8 -quality 90',
          huge:   '-colorspace RGB -flatten -strip -depth 8 -quality 90',
          slideshow:   '-colorspace RGB -flatten -strip -depth 8 -quality 90' }

      validates :image, attachment_presence: true
      validates_attachment_content_type :image, :content_type => /^image\/.*$/
    end

  end
end
