module Frame
  
  # Include in models that require route regeneration when destroyed or updated.
  module GeneratesRoutes
    extend ActiveSupport::Concern
    
    included do
      after_save :reload_routes
      after_destroy :reload_routes
    end
    
    private
      def reload_routes
        
        Frame.routing_table(I18n.locale).reload!
      end
    
  end
end