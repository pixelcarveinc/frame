Frame::Engine.routes.draw do
  concern :multidestroyable do
    delete :index, on: :collection, action: :multi_destroy
  end

  resources :externals, only: [:edit, :update]

  resources :blogs, only: [:edit, :update] do
    resources :posts, except: [:show], concerns: :multidestroyable
  end

  resources :maps, only: [:edit, :update] do
    resources :locations, except: [:show], concerns: :multidestroyable
  end
  resources :galleries, only: [:edit, :update] do
    resources :images, except: [:show], concerns: :multidestroyable
  end
  resources :contacts, only: [:edit, :update] do
    resources :messages, except: [:new, :edit, :update],
      concerns: :multidestroyable
  end
  resources :links, except: [:show]

  #match "/pages/new/:type", to: 'Pages#new', as: 'new_page_type'
  resources :pages, except: [:index, :show] do
    collection do
      get 'new/:type', to: 'pages#new', as: 'new_page_type'
    end
  end
end
