class PlainTextValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    if value =~ /<[^ ][^>]+>/
      record.errors[attribute] << (options[:message] || "may not contain HTML")
    end
  end
end