
# Reworked from https://github.com/perfectline/validates_url to use standard
# URI library and Rails 4 style validations.
class UrlValidator < ActiveModel::EachValidator
  def initialize(options)
    options.reverse_merge!(:schemes => %w(ftp http https mailto tel))
    options.reverse_merge!(:message => "is not a valid URL")
    super(options)
  end

  def validate_each(record, attribute, value)
    schemes = [*options.fetch(:schemes)].map(&:to_s)
    begin
      uri = URI.parse(value)
      unless uri && schemes.include?(uri.scheme)
        record.errors[attribute] << options.fetch(:message)
      end
    rescue URI::Error
      record.errors[attribute] << options.fetch(:message)
    end
  end
end
