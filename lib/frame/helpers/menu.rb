module Frame
  module Helpers
    module MenuHelper
      extend ActiveSupport::Concern
      
      # Render some or all of link structure as a menu.
      #
      # options:
      # :active        - Currently active link
      # :from          - Link or depth to start building menu from.
      # :depth         - How many menu levels to render, defaults to 1, pass -1 for no limit
      # :active_branch - If true, render only the active branch (ie. breadcrumbs), depth is ignored, active is required
      # :partial       - Partial template to use for rendering menu levels
      # :root          - Name of root element containing menu, defaults to ul
      # 
      # All other options are treated as HTML attributes for root element.
      #
      #
      # Examples:
      #
      # Render root menu:
      # link.menu active: link
      #
      # Render entire menu:
      # link.menu active: link, depth: -1
      #
      # Render sub-menu for a node with children:
      # link.menu from: link 
      #
      # Render super menu for a node:
      # link.menu active: link, from: link.parent
      #
      # Render breadcrumbs:
      # link.menu active: link, branch_only: true
      #
      #
      # Example menu partial (in HAML markup):
      #
      # %li
      #   - if link == active
      #     %strong= link.page.title
      #   - else
      #     = link_to link.page.title, link.slug_path
      #   %ul
      #     = children
      #
      # 
      # Example breadcrumb partial (in HAML markup):
      #
      # %li
      #   - if link == active
      #     %strong= link.page.title
      #   - else
      #     = link_to link.page.title, link.slug_path
      # = children
      #
      def menu(options = {})
        menu_options = [:active, :from, :depth, :active_branch, :partial, :root]

        options = {depth: 1, root: :ul}.merge options
        
        tag_options = options.reject { |k, v| menu_options.include? k }
        options.keep_if { |k, v| menu_options.include? k }
  
        col = menu_collection(options)

        render_menu col, options, tag_options
      end
      
      
      private
      
        # Get a collection of links for menu based on passed options.
        def menu_collection(options = {})
          if options[:active_branch]
            raise 'must specify active link when active_branch is true' unless options[:active]
            active_filter decorated_collection, options[:active]
          else
            from = options[:from]
            max_depth = options[:depth] || 1
  
            if from
              ancestry_filter decorated_collection, from, max_depth
            else
              depth_filter decorated_collection, max_depth
            end
          end
        end
        
        def is_ancestor?(target, ancestor, max_depth = -1)
          target.ancestry =~ /^#{ancestor.ancestry}.+/ &&
            (max_depth < 0 || target.depth < ancestor.depth + max_depth + 1)
        end
        
        def active_filter(collection, active)
          ancestors = active.ancestry ? active.ancestry.split('/').collect { |id| id.to_i } : []
          
          collection.keys.reduce({}) do |memo, link|
            if link == active || ancestors.include?(link.id)
              memo[link] = active_filter collection[link], active
            end
            memo
          end
        end
        
        def ancestry_filter(collection, from, max_depth)
          parents = from.ancestry ? from.ancestry.split('/').collect { |id| id.to_i } : []
          parents << from.id
          
          collection.keys.reduce({}) do |memo, link|
            if (link.depth <= from.depth && parents.include?(link.id))
              # if depth is less than from and part of ancestry, descend
              memo = ancestry_filter collection[link], from, max_depth
            elsif is_ancestor?(link, from, max_depth)
              # if child of from, merge
              memo[link] = ancestry_filter collection[link], from, max_depth
            end
            memo
          end
        end
        
        def depth_filter(collection, max_depth, current_depth = 1)
          return {} if max_depth > -1 && current_depth > max_depth
          
          collection.keys.reduce({}) do |memo, link|
            memo[link] = depth_filter collection[link], max_depth, current_depth + 1
            memo
          end  
        end
        
        # Render menu recursively using either passed partial or default markup.
        def render_menu(collection, options = {}, tag_options = {})
          active = options[:active]
          

          if options[:partial]
            h.content_tag options[:root], tag_options do
              collection.keys.collect do |link|
                
                if collection[link].present? 
                  h.render partial: options[:partial], locals: {
                    link: link, active: active, children: render_menu(collection[link], options) 
                  }
                else
                  h.render partial: options[:partial], locals: {
                    link: link, active: active
                  }
                end
              end.join('').html_safe
            end
          else
            h.content_tag options[:root], tag_options do
              collection.keys.collect do |link|
                h.content_tag :li do |li|
                  if link == active
                    buff = h.content_tag :strong, link.page.title
                  else
                    begin
                      buff = h.link_to link.page.title, link.slug_path
                    rescue SluggifiedError
                      if link.page.link_url
                        # page is an external link
                        buff = h.link_to link.page.title, link.page.link_url, target: link.page.link_target
                      else
                        # page doesn't route, don't render it
                        buff = ''
                      end
                    end
                  end
                  buff + render_menu(collection[link], options)
                end
              end.join('').html_safe
            end
          end
        end
        
    end
  end
end