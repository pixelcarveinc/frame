module Frame
  module Helpers
    module RoutingTableHelper
      extend ActiveSupport::Concern
      
      # Render the routing table for JavaScript usage.
      #
      # Options:
      # depth       - How many levels deep to render, default is infinite.
      def routing_table(options = {})
        depth = options[:depth] || Float::INFINITY
        
        #byebug

        unless options[:locale].present?
          options[:locale] = :en
        end

        # remove routes that point to content
        table = Frame.routing_table(options[:locale]).routes[:get].select { |k, v| !v[:content] }
        

        # transform hash into a hash of slug => page type
        table.keys.reduce({}) do |memo, slug|
          # count depth by number of slashes in slug
          next memo if slug.count('/') > depth - 1
          
          type = table[slug][:link].page.type
          memo[slug] = type.blank? ? 'Default' : type.demodulize 
          memo
        end
      end
              
    end
  end
end