
module Frame
  class Engine < ::Rails::Engine
    isolate_namespace Frame
    
    # make some configuration options available through Rails.application.config
    config.frame = ActiveSupport::OrderedOptions.new
    config.frame.page_types = %w(Frame::Home Frame::Blog Frame::Form Frame::Gallery Frame::Contact Frame::External)
  end
end
