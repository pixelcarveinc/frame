require 'paperclip'

module Frame
  class Seeder
    def initialize
      @pages = {}
      @links = []
      @ids = {}

      # Register file path adapter so file paths in sitemap.yml can be used as
      # attachments.  Done during initialization so adapter isn't available to
      # rest of application.
      Paperclip.io_adapters.register FilePathAdapter do |target|
        target.is_a?(String) && !target.empty?
      end
    end
    
    def seed(file)
      pages = YAML.load_file file
      pages['sitemap'].each { |page| create_pages page }
      
      @links.each do |link|
        create_link link, link.parent
      end
    end
  
    private

      def attachment_path
        @attachment_path ||= Rails.root.join 'db', 'files'
      end
      
      def create_pages(attrs, parent = nil)
        
        if attrs.is_a? SeederLink
          # link after pages have been created
          attrs.parent = parent
          @links << attrs
        else
          id = attrs.delete 'id'
          title = attrs.delete 'title'
          children = attrs.delete 'children'
          
          # TODO need specific lines in YAML file where errors are occuring
          raise 'Page missing title' if title.nil?
          
          puts "Generating page: #{title} - #{attrs.inspect} ----"

          page = Frame::Page.create!({title: title}.merge(attrs))
          
          identify_page(page, id) if id
          
          parent = Frame::Link.create! page: page, parent: parent
          
          children.each {|c| create_pages c, parent } if children
        end 
      end
      
      # Store page with unique id.
      def identify_page(page, id)
        raise "Duplicate id: #{id}" if @ids[id]
        @ids[id] = page
      end
      
      def create_link(link, parent)
        page = @ids[link.id]
        raise "Page with id #{link.id} not found" unless page
        new_parent = Frame::Link.create! page: page, parent: parent
        link.children.each { |c| create_link c, new_parent } if link.children
      end
  end
  
  # Denotes a link to be made to a page.
  class SeederLink
    attr_accessor :id, :children, :parent
  end
  
  # Enables passing strings pointing to files as a Paperclip attachment.
  class FilePathAdapter < ::Paperclip::FileAdapter
    
    def initialize(filepath)
      filepath = Rails.root.join(filepath) unless filepath.is_a?(Pathname)
      super File.new(filepath)
    end
  end
  
end
