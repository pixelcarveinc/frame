module Frame
  class SlugMapper
    attr_accessor :content_routes
    attr_reader :link
    
    def initialize(link, options = {})
      @link = link
      
      options.symbolize_keys!
      
      @base = options[:base] || ''
      @slug = options[:slug] || "#{@base}/#{@link.slug}"
      content_routes = options[:content_routes] || false
    end
    
    def get(path = '', options = {})
      match :get, path, options
    end
    
    def post(path = '', options = {})
      match :post, path, options
    end
    
    def put(path = '', options = {})
      match :put, path, options
    end
    
    def patch(path = '', options = {})
      match :patch, path, options
    end
    
    def delete(path = '', options = {})
      match :delete, path, options
    end
    
    def routes
      @routes ||= {
        get: {},
        post: {},
        put: {},
        patch: {},
        delete: {}
      }
    end
    
    
    private
      def match(method, path, options)
        unless path.is_a? String
          options = path
          path = nil
        end
        
        defaults = {
          action: :show,
          controller: controller(@link),
          content: content_routes,
          defaults: {link_id: @link.id},
          link: @link
        }
        
        slug = @slug.dup
        slug += '/' + path unless path.nil?
        slug.gsub!(/^\/|\/$/, '')
        
        routes[method][slug] = defaults.merge options
      end
    
      # Determine the controller to route to based on page type.
      def controller(link)
        con = 'Frame::PagesController'
        
        unless link.page.type.blank?
          con_name = link.page.type.pluralize + 'Controller'
          con = con_name if controller_exists?(con_name)
        end
        
        con.constantize
      end
    
      # Test if controller exists and optionally if specified action exists on
      # controller.
      def controller_exists?(controller_name, action = nil)
        klass = Module.const_get controller_name
        klass.respond_to?(:action) && (action.nil? || klass.method_defined?(action))
      rescue NameError
        return false
      end
  end
end