module Frame

  def self.routing_table(locale = :en)
    
    @routing_table = {} unless @routing_table.present?

    #byebug

    if @routing_table[locale].present? && @routing_table[locale].reload?
      @routing_table[locale] = RoutingTable.new(locale)
    else
      @routing_table[locale] ||= RoutingTable.new(locale)
    end
    
  end


  class RoutingTable

    def initialize(locale = :en)
      
      @routables = {}
      @locale = locale
    end

    def reload!

      @routes = @slugs = {}

    end

    def reload?
      if @routes.length == 0
        true
      else
        false
      end
    end

    def routes

      @routes = {} unless @routes.present?
 
      unless @routes[@locale]
        @routes[@locale] = {
          get: {},
          post: {},
          put: {},
          patch: {},
          delete: {}
        }


        links = Frame::Link.arrange

        if links.keys.any?
          links.keys.each { |l| map_link l, links[l] }

          # pass through routable but don't generate from slug
          
          add_routes find_routable(links.keys.first).map_routes(links.keys.first, slug: '')
        end

      end
      
      @routes[@locale]
    end

    def slugs
      #reload!
      @slugs = {} unless @slugs.present?

      @slugs[@locale] ||= routes[:get].keys.reduce({}) do |memo, slug|
        next memo if routes[:get][slug][:content]
        memo[routes[:get][slug][:link]] = slug
        memo
      end
    end

    private

      def add_routes(mapped)
        mapped.keys.each do |method|
          @routes[@locale][method].merge! mapped[method]
        end
      end

      # Recursively map link and its children.
      def map_link(link, children, base = '')
        if Rails.application.config.frame.single_slug_mode
          add_routes find_routable(link).map_routes(link, base: '')
        else
          add_routes find_routable(link).map_routes(link, base: base)
        end

        
        children.keys.each { |l| map_link l, children[l], "#{base}/#{link.slug}" }
      end

      # Find specific routable for link or use the default one.
      def find_routable(link)
        unless @routables[link.page.type]
          # default to Routable but if a custom class exists for page type use it
          rtb = 'Frame::Routable'

          unless link.page.type.blank?
            rtb_name = link.page.type + 'Routable'
            begin
              rtb = rtb_name if Module.const_get rtb_name
            rescue NameError
            end
          end

          @routables[link.page.type] = rtb.constantize.new
        end
        @routables[link.page.type]
      end

  end
end
