module Frame
  class Routable
    
    # Map routes for link from base slug.
    def map_routes(link, options = {})
      mapper = SlugMapper.new(link, options)
      #mapper.get
      page_routes mapper
      
      mapper.content_routes = true
      content_routes mapper
      
      mapper.routes
    end
      
    # Route to page via slug.  Default is to route to page controller's show
    # action.  Override to provide alternate action or more routes (ie. POST).
    def page_routes(mapper)
      mapper.get
    end
      
    # Route to content within page.  Default is noop, override to provide
    # routing to page content where required.
    def content_routes(mapper)
      
    end
  end
end