module Frame
  
  # Use as a routing concern for routing to pages via links.
  class Sluggified
    
    # TODO allow options in initialize, specificy additional exclusions
    def call(mapper, options = {})
      
      mapper.get '/',  to: handler, via: :all, as: :link_root
      mapper.match '*slug', to: handler, via: :all,
        constraints: lambda { |req|  req.env["REQUEST_URI"] != "admin" },
        as: :link_slug
      
      # overwritting default link_slug route helpers at this point so they're
      # transparently available to controllers and views
      include_url_helpers
    end
    
    private
      def handler
        @handler ||= SlugRouteHandler.new
      end
      
      # Add link slug path/url helpers to routing helpers.
      def include_url_helpers
        Rails.application.routes.named_routes.module.module_eval do
          
          # TODO calls to build_link_slug should be DRYed up
          
          # Generate a slug based path for link, uses same options as normal
          # path helpers.
          def link_slug_path(link, options = {})

            content = options.delete :content
            # TODO allow content to be a string, model with slug or model without slug (to_s)
            
            build_link_slug link, {
              pre: link_root_path(options),
              post: content ? '/' + content.slug : '',
              trailing_slash: options.delete(:trailing_slash)
            }
          end
          
          # Generate a slug based url for link, uses same options as normal
          # path helpers.
          def link_slug_url(link, options = {})
            content = options.delete :content
            
            build_link_slug link, {
              pre: link_root_url(options),
              post: content ? '/' + content.slug : '',
              trailing_slash: options.delete(:trailing_slash)
            }
          end
          
          private
    
            # All slug route definitions from routing table.
            def slugs
              # important that slugs are only genereated from routing table when
              # required to prevent issues with accessing the db at app init time
              #byebug
              Frame.routing_table(I18n.locale).slugs
            end
          
            # Add a callback function for passed resource, overwrites any
            # existing callback.
            def add_resource_callback(resource, callback)
              resource_callbacks[resource] = callback
            end
            
            # Stored hash of callbacks for resources.
            def resource_callbacks
              @resource_callbacks ||= {}
            end
          
            # Build a link slug from link and root link slug.
            def build_link_slug(link, options)
              
              raise SluggifiedError.new('Invalid link') if link.nil? 
              
              pre = options.delete(:pre) || ''
              post = options.delete(:post) || ''
              
              post += '/' if options[:trailing_slash]
              
              # strip leading slash and split into path component and query/anchor
              pre = pre.sub(/(\?|#).*/, '')
              # trailing slash option if set
              # get the query/anchor from the last match (above)
              post += $~ ? $~[0] : ''
              
              # look up slug for link if it's a model
              link_slug = link.is_a?(Frame::Link) ? slugs[link] : link.to_s
              # error only occurs if a lookup from a model is not possible
              if link_slug.nil?
                raise SluggifiedError.new("Cannot generate slug for link (id: #{link.id})")
              end
              link_slug.dup.gsub!(/^\/|\/$/, '')
              
              # prepare full path
              if link_slug.blank?
                pre + post
              else
                if pre =~ /\/$/
                  pre + link_slug + post
                else
                  pre + '/' + link_slug + post
                end
              end
            end

        end
      end
  end
  
  # Handles redirection from slug routes to link paths
  class SlugRouteHandler
    def call(env)
      request = ActionDispatch::Request.new env
      method = request.method.downcase.to_sym
      if request.subdomain == 'fr' || request.subdomain.include?("fr.")
        I18n.locale = :fr

      else
        I18n.locale = :en

      end
      
      Frame.routing_table(I18n.locale).routes[method] || unsupported_method
      
      slug = request.params[:slug] || ''
      route = Frame.routing_table(I18n.locale).routes[method][slug] || not_found
      
      # modify env, merge route defaults
      env['action_dispatch.request.parameters'].merge! route[:defaults]

      route[:controller].action(route[:action]).call(env)
    end
    
    private
      
      def not_found
        raise ActionController::RoutingError.new('Not Found')
      end
      
      def unsupported_method
        raise ActionController::RoutingError.new('Unsupported Method')
      end
  end
  
  class SluggifiedError < ::StandardError
  end

end