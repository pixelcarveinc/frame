require "frame/engine"
require "frame/seeder"
require "frame/helpers/menu"
require "frame/helpers/routing_table"
require 'frame/routing/routable'
require 'frame/routing/routing_table'
require 'frame/routing/sluggified'
require 'frame/routing/slug_mapper'
require "validators/email_validator"
require "validators/plain_text_validator"
require "validators/url_validator"
# requires below fix various problems when mounting the engine in an app
require "draper"
require "haml"
require "haml_coffee_assets"
require "compass-rails"
require "ancestry"
require "acts_as_list"

module Frame
end
