class CreateFrameMessages < ActiveRecord::Migration
  def change
    create_table :frame_messages do |t|
      t.integer :contact_id
      t.string :name
      t.string :email_address
      t.string :phone_number
      t.string :company_name
      t.string :company_address
      t.text :comments

      t.timestamps
    end
    
    add_index :frame_messages, :contact_id
  end
end
