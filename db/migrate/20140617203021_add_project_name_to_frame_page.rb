class AddProjectNameToFramePage < ActiveRecord::Migration
  def change
    add_column :frame_pages, :project_name, :string
  end
end
