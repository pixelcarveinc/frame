class ChangePublishedDefaultToTrue < ActiveRecord::Migration
  def up
    change_column_default :frame_posts, :published, true
    change_column_default :frame_images, :published, true
  end
  
  def down
    change_column_default :frame_posts, :published, false
    change_column_default :frame_images, :published, false
  end
end
