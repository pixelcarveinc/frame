class CreateFramePages < ActiveRecord::Migration
  def change
    create_table :frame_pages do |t|
      t.string :type
      t.string :title
      t.text :body
      
      t.timestamps
    end
  end
end
