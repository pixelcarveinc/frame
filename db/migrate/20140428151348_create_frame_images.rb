class CreateFrameImages < ActiveRecord::Migration
  def change
    create_table :frame_images do |t|
      t.integer :gallery_id
      t.string :title
      t.string :slug
      t.string :alt
      t.text :body
      t.boolean :published, default: false
      t.integer :position

      t.timestamps
    end
    
    add_attachment :frame_images, :image
    add_index :frame_images, [:gallery_id, :position]
  end
end
