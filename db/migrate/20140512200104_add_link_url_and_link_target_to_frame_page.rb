class AddLinkUrlAndLinkTargetToFramePage < ActiveRecord::Migration
  def change
    add_column :frame_pages, :link_url, :string
    add_column :frame_pages, :link_target, :string
  end
end
