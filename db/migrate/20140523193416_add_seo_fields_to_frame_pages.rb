class AddSeoFieldsToFramePages < ActiveRecord::Migration
  def change
    add_column :frame_pages, :meta_description, :string
    add_column :frame_pages, :head_title, :string
  end
end
