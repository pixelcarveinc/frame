class CreateFrameLinks < ActiveRecord::Migration
  def change
    create_table :frame_links do |t|
      t.string :ancestry
      t.integer :ancestry_depth, default: 0
      t.integer :position
      t.integer :page_id
      t.string :slug

      t.timestamps
    end
    
    add_index :frame_links, [:ancestry, :position]
    add_index :frame_links, :page_id
  end
end
