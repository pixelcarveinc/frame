class CreateFramePosts < ActiveRecord::Migration
  def change
    create_table :frame_posts do |t|
      t.integer :blog_id
      t.string :category
      t.string :title
      t.string :slug
      t.text :body
      t.boolean :published, default: false
      t.datetime :publish_date

      t.timestamps
    end
    
    add_index :frame_posts, :blog_id
  end
end
