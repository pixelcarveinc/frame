class AddContactPageFields < ActiveRecord::Migration
  def change
    change_table :frame_pages do |t|
      t.string :emails
    end
  end
end
