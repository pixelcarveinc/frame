require 'test_helper'

module Frame
  class MessageMailerTest < ActionMailer::TestCase
    
    setup do
      @message = frame_messages(:message1)
    end
    
    test "message" do
      email = MessageMailer.message_email @message
      body = email.body.to_s
    
      assert_equal @message.contact.emails.split(','), email.to
      assert_equal 'New Message From Contact Form', email.subject
      
      assert_match @message.name, body
      assert_match @message.email_address, body
      assert_match @message.phone_number, body
      assert_match @message.company_name, body
      assert_match @message.company_address, body
      assert_match @message.comments, body
    end
  end
end
