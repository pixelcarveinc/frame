require 'test_helper'

module Frame
  class LinksHelperTest < ActionView::TestCase
    
    test "slug path helper" do
      assert_equal '/site', link_slug_path(frame_links(:home))
      assert_equal '/site/featured', link_slug_path(frame_links(:featured))
      assert_equal '/site/featured/widget1', link_slug_path(frame_links(:featured_widget1))
      assert_equal '/site/featured?order=desc&sort=price', link_slug_path(frame_links(:featured), sort: 'price', order: 'desc')
      assert_equal '/site/featured/', link_slug_path(frame_links(:featured), trailing_slash: true)
      assert_equal '/site/featured#test', link_slug_path(frame_links(:featured), anchor: 'test')
    end
    
    test "slug url helper" do
      assert_equal 'http://test.host/site', link_slug_url(frame_links(:home))
      assert_equal 'http://test.host/site/featured', link_slug_url(frame_links(:featured))
      assert_equal 'http://test.host/site/featured/widget1', link_slug_url(frame_links(:featured_widget1))
      assert_equal 'http://test.host/site/featured?order=desc&sort=price', link_slug_url(frame_links(:featured), sort: 'price', order: 'desc')
      assert_equal 'http://test.host/site/featured/', link_slug_url(frame_links(:featured), trailing_slash: true)
      assert_equal 'http://test.host/site/featured#test', link_slug_url(frame_links(:featured), anchor: 'test')
    end
    
    test "slug path helper with content" do
      assert_equal '/site/featured/product-1', link_slug_path(frame_links(:featured), content: frame_posts(:post1))
      assert_equal '/site/featured/product-1?order=desc&sort=price', link_slug_path(frame_links(:featured), content: frame_posts(:post1), sort: 'price', order: 'desc')
      assert_equal '/site/featured/product-1/', link_slug_path(frame_links(:featured), content: frame_posts(:post1), trailing_slash: true)
      assert_equal '/site/featured/product-1#test', link_slug_path(frame_links(:featured), content: frame_posts(:post1), anchor: 'test')
    end
    
    test "slug url helper with content" do
      assert_equal 'http://test.host/site/widgets/desert', link_slug_url(frame_links(:widgets), content: frame_images(:image1))
      assert_equal 'http://test.host/site/widgets/desert?order=desc&sort=price', link_slug_url(frame_links(:widgets), content: frame_images(:image1), sort: 'price', order: 'desc')
      assert_equal 'http://test.host/site/widgets/desert/', link_slug_url(frame_links(:widgets), content: frame_images(:image1), trailing_slash: true)
      assert_equal 'http://test.host/site/widgets/desert#test', link_slug_url(frame_links(:widgets), content: frame_images(:image1), anchor: 'test')
    end
    
  end
end
