require 'test_helper'

module Frame
  class GalleriesControllerTest < ActionController::TestCase
    setup do
      @gallery = frame_pages(:widgets)
    end

    test "should get show" do
      @routes = Rails.application.routes
      
      get :show, use_route: :frame, link_id: @gallery.links[0].id
      assert_response :success
      assert assigns(:gallery)
    end

    test "should get edit" do
      get :edit, use_route: :frame, id: @gallery
      assert_response :success
    end

    test "should update gallery" do
      patch :update, use_route: :frame, id: @gallery, gallery: { body: @gallery.body, title: @gallery.title, type: @gallery.type }
      assert_redirected_to links_path
    end
  end
end
