require 'test_helper'

module Frame
  class ExternalsControllerTest < ActionController::TestCase
    setup do
      @external = frame_pages(:pricing)
    end

    test "should get edit" do
      get :edit, use_route: :frame, id: @external
      assert_response :success
    end

    test "should update blog" do
      patch :update, use_route: :frame, id: @external, external: external_params
      assert_redirected_to links_path
      assert_attributes assigns(:external), external_params
    end
    
    private
      def external_params
        { title: @external.title, type: @external.type, link_url: @external.link_url, link_target: @external.link_target }
      end
      
  end
end
