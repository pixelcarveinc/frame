require 'test_helper'

module Frame
  class BlogsControllerTest < ActionController::TestCase
    setup do
      @blog = frame_pages(:featured)
    end

    test "should get show" do
      @routes = Rails.application.routes
      
      get :show, use_route: :frame, link_id: @blog.links[0].id
      assert_response :success
      assert assigns(:blog)
    end

    test "should get edit" do
      get :edit, use_route: :frame, id: @blog
      assert_response :success
    end

    test "should update blog" do
      patch :update, use_route: :frame, id: @blog, blog: { body: @blog.body, title: @blog.title, type: @blog.type }
      assert_redirected_to links_path
    end
  end
end
