require 'test_helper'

module Frame
  class LinksControllerTest < ActionController::TestCase
    
    setup do
      @link = frame_links(:home)
    end

    test "should get index" do
      get :index, use_route: :frame
      assert_response :success
      assert_not_nil assigns(:links)
    end

    test "should get new" do
      get :new, use_route: :frame
      assert_response :success
    end

    test "should create link" do
      assert_difference('Link.count') do
        post :create, use_route: :frame, link: link_params.merge({ parent_id: @link.id })
      end

      assert_redirected_to link_path(assigns(:link))
      assert_attributes assigns(:link), link_params
    end

    test "should get edit" do
      get :edit, use_route: :frame, id: @link
      assert_response :success
    end

    test "should update link" do
      patch :update, use_route: :frame, id: @link, link: link_params.merge({ parent_id: @link.parent_id })
      assert_redirected_to link_path(assigns(:link))
    end

    test "should destroy link" do
      assert_difference('Link.count', -1) do
        delete :destroy, use_route: :frame, id: @link
      end

      assert_redirected_to links_path
    end
    
    test "should update link parent" do
      @parent = frame_links(:contact)
      
      patch :update, format: :json, use_route: :frame, id: @link, link: { parent_id: @parent.id }
      assert :success
      assert_equal @parent, @link.reload.parent
    end
    
    test "should update link parent to nil" do
      @link = frame_links(:widgets_widget1)
      patch :update, format: :json, use_route: :frame, id: @link, link: { parent_id: nil }
      assert :success
      assert_equal nil, @link.reload.parent
    end
    
    private
      def link_params
        { page_id: @link.page_id, position: @link.position, slug: @link.slug }
      end
  end
end
