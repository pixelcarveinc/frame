require 'test_helper'

module Frame
  class PagesControllerTest < ActionController::TestCase
    
    setup do
      @page = frame_pages(:home)
    end

    test "should get show" do
      @routes = Rails.application.routes
      
      get :show, use_route: :frame, link_id: @page.links[0].id
      assert_response :success
      assert assigns(:page)
    end

    test "should get new" do
      get :new, use_route: :frame
      assert_response :success
    end

    test "should create page" do
      assert_difference('Page.count') do
        post :create, use_route: :frame, page: page_params
      end

      assert_redirected_to links_path
      assert_attributes assigns(:page), page_params
    end

    test "should get edit" do
      get :edit, use_route: :frame, id: @page
      assert_response :success
    end

    test "should update page" do
      patch :update, use_route: :frame, id: @page, page: page_params
      assert_redirected_to links_path
    end

    test "should destroy page" do
      assert_difference('Page.count', -1) do
        delete :destroy, use_route: :frame, id: @page
      end

      assert_redirected_to links_path
    end
    
    
    private
      def page_params
        { body: @page.body, title: @page.title, type: @page.type }
      end
    
  end
end
