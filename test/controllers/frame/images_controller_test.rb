require 'test_helper'

module Frame
  class ImagesControllerTest < ActionController::TestCase
    setup do
      @image = frame_images(:image1)
      @gallery = @image.gallery
    end

    test "should get show" do
      @routes = Rails.application.routes
      
      get :show, use_route: :frame, link_id: @gallery.links[0], image_id: @image.id
      assert_response :success
      assert assigns(:gallery)
      assert assigns(:image)
    end

    test "should get index" do
      get :index, use_route: :frame, gallery_id: @gallery
      assert_response :success
      assert_not_nil assigns(:images)
    end

    test "should get new" do
      get :new, use_route: :frame, gallery_id: @gallery
      assert_response :success
    end

    test "should create image" do
      assert_difference('Image.count') do
        post :create, use_route: :frame, gallery_id: @gallery, image: image_params
      end

      assert_redirected_to gallery_images_path(@gallery)
      assert_attributes assigns(:image), image_params.delete_if { |k,v| k == :image }
    end

    test "should get edit" do
      get :edit, use_route: :frame, gallery_id: @gallery, id: @image
      assert_response :success
    end

    test "should update image" do
      patch :update, use_route: :frame, gallery_id: @gallery, id: @image, image: image_params
      assert_redirected_to gallery_images_path(@gallery)
      assert_attributes assigns(:image), image_params.delete_if { |k,v| k == :image }
    end

    test "should destroy image" do
      assert_difference('Image.count', -1) do
        delete :destroy, use_route: :frame, gallery_id: @gallery, id: @image
      end

      assert_redirected_to gallery_images_path(@gallery)
    end
  
    test "should multi destroy images" do
      assert_difference('Image.count', -2) do
        delete :multi_destroy, gallery_id: @gallery, id: [@image.id, frame_images(:image2).id]
      end
      
      assert_redirected_to gallery_images_path(@gallery)
    end
    
    private
      def image_params
        {
          alt: @image.alt,
          body: @image.body,
          published: @image.published,
          position: @image.position,
          title: @image.title,
          image: fixture_file_upload('/files/test1.jpg', 'image/jpeg')
        }
      end
  end
end
