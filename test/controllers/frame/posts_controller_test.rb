require 'test_helper'

module Frame
  class PostsControllerTest < ActionController::TestCase
    
    setup do
      @post = frame_posts(:post1)
      @blog = @post.blog
    end

    test "should get show" do
      @routes = Rails.application.routes
      
      get :show, use_route: :frame, link_id: @blog.links[0], post_id: @post.id
      assert_response :success
      assert assigns(:blog)
      assert assigns(:post)
    end

    test "should get index" do
      get :index, use_route: :frame, blog_id: @blog
      assert_response :success
      assert_not_nil assigns(:posts)
    end

    test "should get new" do
      get :new, use_route: :frame, blog_id: @blog
      assert_response :success
    end

    test "should create post" do
      assert_difference('Post.count') do
        post :create, use_route: :frame, blog_id: @blog, post: post_params
      end

      assert_redirected_to blog_posts_path(@blog)
      assert_attributes assigns(:post), post_params
    end

    test "should get edit" do
      get :edit, use_route: :frame, blog_id: @blog, id: @post
      assert_response :success
    end

    test "should update post" do
      patch :update, use_route: :frame, blog_id: @blog, id: @post, post: post_params
      assert_redirected_to blog_posts_path(@blog)
    end

    test "should destroy post" do
      assert_difference('Post.count', -1) do
        delete :destroy, use_route: :frame, blog_id: @blog, id: @post
      end

      assert_redirected_to blog_posts_path(@blog)
    end
  
    test "should multi destroy posts" do
      assert_difference('Post.count', -2) do
        delete :multi_destroy, blog_id: @blog, id: [@post.id, frame_posts(:post2).id]
      end
      
      assert_redirected_to blog_posts_path(@blog)
    end
    
    private
      def post_params
        { blog_id: @post.blog_id, body: @post.body, category: @post.category, publish_date: @post.publish_date, published: @post.published, title: @post.title }
      end
  end
end
