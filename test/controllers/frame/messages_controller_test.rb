require 'test_helper'

module Frame
  class MessagesControllerTest < ActionController::TestCase
    setup do
      @routes = Frame::Engine.routes
      @message = frame_messages(:message1)
      @contact = @message.contact
      @link = @contact.links.first
    end
  
    test "should get index" do
      get :index, use_route: :frame, contact_id: @contact
      assert_response :success
      assert_not_nil assigns(:messages)
    end
  
    test "should get new" do
      @routes = Rails.application.routes
      
      get :new, use_route: :frame, link_id: @link
      assert_response :success
      assert_not_nil assigns(:message)
    end
      
    test "should create message" do
      @routes = Rails.application.routes
      
      assert_difference('Message.count') do
        post :create, use_route: :frame, link_id: @link, message: message_params
      end
  
      assert_redirected_to @link.decorate.slug_path
      assert_attributes assigns(:message), message_params
    end
  
    test "should send message email" do
      @routes = Rails.application.routes
      
      assert_difference('ActionMailer::Base.deliveries.size') do
        post :create, use_route: :frame, link_id: @link, message: message_params
      end
    end

    test "should show message" do
      get :show, use_route: :frame, contact_id: @contact, id: @message
      assert_response :success
      assert assigns(:message)
    end
  
    test "should destroy message" do
      assert_difference('Message.count', -1) do
        delete :destroy, use_route: :frame, contact_id: @contact, id: @message
      end
  
      assert_redirected_to contact_messages_path(@contact)
    end
  
    test "should multi destroy messages" do
      assert_difference('Message.count', -2) do
        delete :multi_destroy, contact_id: @contact, id: [@message.id, frame_messages(:message2).id]
      end
      
      assert_redirected_to contact_messages_path(@contact)
    end
    
    private
      def message_params
        { contact_id: @message.contact_id, comments: @message.comments, company_address: @message.company_address, company_name: @message.company_name, email_address: @message.email_address, name: @message.name, phone_number: @message.phone_number }
      end
  end
end
