require 'test_helper'

module Frame
  class ContactsControllerTest < ActionController::TestCase
    
    setup do
      @routes = Engine.routes
      @contact = frame_pages(:contact)
      @message = frame_messages(:message1)
    end

    test "should get edit" do
      get :edit, use_route: :frame, id: @contact
      assert_response :success
    end

    test "should update contact" do
      patch :update, use_route: :frame, id: @contact, contact: contact_params
      assert_redirected_to links_path
      assert_attributes @contact, contact_params
    end
    
    private
      def contact_params
        { body: @contact.body, title: @contact.title, type: @contact.type, emails: @contact.emails }
      end

      def message_params
        { contact_id: @message.contact_id, comments: @message.comments, company_address: @message.company_address, company_name: @message.company_name, email_address: @message.email_address, name: @message.name, phone_number: @message.phone_number }
      end
  end
end
