Rails.application.routes.draw do
  
  concern :sluggified, Frame::Sluggified.new
  
  devise_for :users

  mount Frame::Engine => "/frame"
  mount Drive::Engine => "/drive"
  
  scope :site do
    concerns :sluggified
  end
  
  get 'admin/activity', as: :activity
  
  root to: 'home#index'
end
