Frame::Engine.load_seed
Drive::Engine.load_seed

# clean paperclip attachments
system_dir = Rails.root.join('public', 'system')
FileUtils.rm_r(system_dir) if system_dir.exist?
  

# Drive
files_path = Rails.root.join 'db', 'files'

drive = Drive::Folder.find 1
drive.files.create! upload: File.new(files_path.join('lorem ipsum.txt'))
drive.files.create! upload: File.new(files_path.join('Humpback Whale.jpg'))
drive.files.create! upload: File.new(files_path.join('vlcsnap-800362.png'))

apps = drive.children.create! name: 'Apps'

pdfs = drive.children.create! name: 'PDFs', fixed: true
pdfs.files.create! upload: File.new(files_path.join('mod_rewrite_cheat_sheet.pdf'))

images = drive.children.create! name: 'Images'
images.files.create! upload: File.new(files_path.join('Tulips.jpg'))

images.children.create! name: 'New'

old = images.children.create! name: 'Old'
old.files.create! upload: File.new(files_path.join('Desert.jpg'))

projects = drive.children.create! name: 'Projects'

foglers = projects.children.create! name: 'Foglers'
 
cms = foglers.children.create! name: 'CMS'
cms.files.create! upload: File.new(files_path.join('countries.txt'))


# Frame
seeder = Frame::Seeder.new
seeder.seed Rails.root.join('db', 'sitemap.yml')

contact = Frame::Page.find_by_title 'Contact'
contact.messages.create!({ name: 'Ted Testman', email_address: 'test@example.net',
  phone_number: '4165550555', company_name: 'Test Co.', company_address: '123 Fake St.',
  comments: 'I love your site' })
contact.messages.create!({ name: 'Tina Testman', email_address: 'test2@example.net', 
  comments: 'Please contact me' })


# Users
User.create! email: 'test@example.net',  password: 'password', password_confirmation: 'password', role: 'editor'
manager = User.create! email: 'test2@example.net', password: 'password', password_confirmation: 'password', role: 'admin'


# set all activities owner to manager
PublicActivity::Activity.all.each { |a| a.update! owner: manager }