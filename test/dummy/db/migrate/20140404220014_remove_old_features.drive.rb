# This migration comes from drive (originally 20140401214857)
class RemoveOldFeatures < ActiveRecord::Migration
  def change
    remove_index :drive_folders, name: 'idx_drive_folders_protected'
    remove_column :drive_folders, :display_mode
    remove_column :drive_folders, :protected
  end
end
