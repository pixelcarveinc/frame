# This migration comes from drive (originally 20130430161618)
class AddFixedToDriveFolders < ActiveRecord::Migration
  def change
    add_column :drive_folders, :fixed, :boolean, default: false
  end
end
