# This migration comes from drive (originally 20130430134740)
class AddProtectedToDriveFolders < ActiveRecord::Migration
  def change
    add_column :drive_folders, :protected, :boolean, default: false
    add_index :drive_folders, :protected, name: 'idx_drive_folders_protected'
  end
end
