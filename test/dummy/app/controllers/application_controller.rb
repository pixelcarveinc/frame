class ApplicationController < ActionController::Base
  include Frame::FrontendActions

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  layout :pick_layout

  private

    def authenticate!
      authenticate_user!
    end

    def pick_layout
      is_frontend?(self.class, action_name) ? 'frontend' : nil
    end
end
