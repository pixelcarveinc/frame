class AdminController < ApplicationController
  def activity
    @activities = PublicActivity::Activity.where('owner_id = :id OR recipient_id = :id', id: current_user.id).order('created_at DESC').limit(25)
  end
end
