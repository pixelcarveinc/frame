require 'test_helper'

module Frame
  class BlogDecoratorTest < Draper::TestCase
    setup do
      @blog = frame_pages(:featured).decorate
    end
    
    test "decorateds associated posts" do
      assert @blog.posts.first.is_a? PostDecorator
    end
  end
end
