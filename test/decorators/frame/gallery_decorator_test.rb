require 'test_helper'

module Frame
  class GalleryDecoratorTest < Draper::TestCase
    setup do
      @gallery = frame_pages(:widgets).decorate
    end
    
    test "decorateds associated images" do
      assert @gallery.images.first.is_a? ImageDecorator
    end
  end
end
