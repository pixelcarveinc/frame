require 'test_helper'

module Frame
  class LinkDecoratorTest < Draper::TestCase
    
    setup do
      @parent = frame_links(:widgets).decorate
      @child = frame_links(:featured_widget1).decorate
    end
    
    test "decorates associated page" do
      assert @parent.page.is_a? PageDecorator
    end
    
    test "decorates associated parent" do
      assert @child.parent.is_a? LinkDecorator
    end
    
    test "decorates associated root" do
      assert @child.root.is_a? LinkDecorator
    end
    
    test "decorated associated ancestors" do
      assert @child.ancestors.first.is_a? LinkDecorator
    end
    
    test "decorated associated children" do
      assert @parent.children.first.is_a? LinkDecorator
    end
    
    test "decorated associated siblings" do
      assert @parent.siblings.first.is_a? LinkDecorator
    end
    
    test "decorated associated descendants" do
      assert @parent.descendants.first.is_a? LinkDecorator
    end
    
    test "decorated associated subtree" do
      assert @parent.subtree.first.is_a? LinkDecorator
    end
    
    test "decorated associated higher_item" do
      assert @parent.higher_item.is_a? LinkDecorator
    end
    
    test "decorated associated higher_items" do
      assert @parent.higher_items.first.is_a? LinkDecorator
    end
    
    test "decorated associated lower_item" do
      assert @parent.lower_item.is_a? LinkDecorator
    end
    
    test "decorated associated lower_items" do
      assert @parent.lower_items.first.is_a? LinkDecorator
    end
    
  end
end
