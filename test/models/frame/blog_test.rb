require 'test_helper'

module Frame
  class BlogTest < ActiveSupport::TestCase
    setup do
      @blog = frame_pages(:featured)
    end
    
    test "destroys associated posts" do
      assert_difference('Post.count', -2) do
        @blog.destroy
      end
    end
  end
end
