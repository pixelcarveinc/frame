require 'test_helper'

module Frame
  class PageTest < ActiveSupport::TestCase
    
    setup do
      @page = frame_pages(:home)
    end
    
    test "title is required" do
      refute @page.update title: nil
      assert @page.errors.include? :title
    end
    
    test "destroys associated links" do
      assert_difference('Link.count', -1) do
        @page.destroy
      end
    end
    
    test "title change updates links" do
      link = @page.links.first
      @page.update title: 'Changed'
      assert_equal 'changed', link.reload.slug
    end
    
    test "includes slug in json from link when passed" do
      link = @page.links.first
      json = @page.as_json(link: link)
      assert_equal link.slug, json['slug']
    end
    
    test "includes type in json" do
      json = @page.as_json
      assert_equal 'Home', json['type']
      json = frame_pages(:featured).as_json
      assert_equal 'Blog', json['type']
    end
  end
end
