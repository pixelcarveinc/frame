require 'test_helper'

module Frame
  class MessageTest < ActiveSupport::TestCase
    setup do
      @message = frame_messages(:message1)
    end
    
    test "belongs to contact" do
      assert_equal frame_pages(:contact), @message.contact
    end
    
    test "contact_id is required" do
      refute @message.update contact_id: nil
    end
    
    test "name is required" do
      refute @message.update name: nil
    end
    
    test "email_address is required" do
      refute @message.update email_address: nil
    end
    
    test "email_address must be email" do
      refute @message.update email_address: 'invalid'
    end
    
    test "comments is required" do
      refute @message.update comments: nil
    end
    
    test "html in comments is not allowed" do
      refute @message.update comments: '<p>invalid</p>'
    end
  end
end
