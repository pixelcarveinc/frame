require 'test_helper'

module Frame
  class ExternalTest < ActiveSupport::TestCase
    setup do
      @page = frame_pages(:pricing)
    end
    
    test "defaults link_target to _blank" do
      @page.update link_target: nil
      assert_equal '_blank', @page.link_target
    end
    
    test "link_target must be one of valid types" do
      Frame::External::LINK_TARGETS.each do |t|
        assert @page.update link_target: t
      end
      refute @page.update link_target: 'invalid'
      assert @page.errors.include? :link_target
    end
    
    test "link_url must be present" do
      refute @page.update link_url: nil
      assert @page.errors.include? :link_url
    end
    
    test "link_url must be a url" do
      refute @page.update link_url: 'invalid'
      assert @page.errors.include? :link_url
    end
    
    test "link_url only allows common schemes" do
      assert @page.update link_url: 'ftp://somewhere.com/a_file.zip'
      assert @page.update link_url: 'mailto:bob@example.net'
      assert @page.update link_url: 'http://google.com'
      assert @page.update link_url: 'https://images.google.com'
      refute @page.update link_url: 'pop:example.net'
      refute @page.update link_url: 'gopher:example.net'
    end
  end
end
