require 'test_helper'

module Frame
  class PostTest < ActiveSupport::TestCase
    
    setup do
      @post = frame_posts(:post1)
      @blog = @post.blog
    end
    
    test "title is required" do
      refute @post.update title: nil
      assert @post.errors.include? :title
    end
    
    test "requires associated blog" do
      refute @post.update blog_id: nil
      assert @post.errors.include? :blog
    end
    
    test "auto generate slug" do
      slug = @post.title.create_id
      @post.update slug: nil
      assert_equal slug, @post.slug
    end
    
    test "generate unqiue slug at blog" do
      post = @blog.posts.create title: @post.title, slug: @post.slug 
      assert_equal 'product-3', post.slug
    end
  end
end
