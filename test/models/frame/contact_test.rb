require 'test_helper'

module Frame
  class ContactTest < ActiveSupport::TestCase
    setup do
      @contact = frame_pages(:contact)
    end
    
    test "destroys associated messages" do
      assert_difference('Message.count', -2) do
        @contact.destroy
      end
    end
    
    test "emails is required" do
      refute @contact.update emails: nil
      assert @contact.errors.include? :emails
    end
  end
end
