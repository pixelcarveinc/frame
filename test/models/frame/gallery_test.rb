require 'test_helper'

module Frame
  class GalleryTest < ActiveSupport::TestCase
    setup do
      @gallery = frame_pages(:widgets)
    end
    
    test "destroys associated images" do
      assert_difference('Image.count', -3) do
        @gallery.destroy
      end
    end
  end
end
