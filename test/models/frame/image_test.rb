require 'test_helper'

module Frame
  class ImageTest < ActiveSupport::TestCase
    
    setup do
      @image = frame_images(:image1)
      @gallery = @image.gallery
    end
    
    test "auto generate title" do
      @image.update title: nil
      assert_equal 'Desert', @image.title
    end
    
    test "requires attached image" do
      refute @image.update image: nil
      assert @image.errors.include? :image
    end
    
    test "requires associated gallery" do
      refute @image.update gallery_id: nil
      assert @image.errors.include? :gallery
    end
    
    test "requires image file type" do
      refute @image.update image: test_text_file
      assert @image.errors.include? :image
    end
    
    test "auto generate slug" do
      slug = @image.title.create_id
      @image.update slug: nil
      assert_equal slug, @image.slug
    end
    
    test "generate unqiue slug at gallery" do
      image = @gallery.images.create title: @image.title, slug: @image.slug, image: test_image_file
      assert_equal 'desert-1', image.slug
    end
    
    test "defaults alt to title" do
      @image.update alt: nil
      assert_equal @image.title, @image.alt
    end
    
    private
      def test_image_file
        File.open(Rails.root.join('db', 'files', 'Desert.jpg'))
      end
      
      def test_text_file
        File.open(Rails.root.join('db', 'files', 'countries.txt'))
      end
  end
end
