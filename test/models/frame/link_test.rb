require 'test_helper'

module Frame
  class LinkTest < ActiveSupport::TestCase
    
    setup do
      @link = frame_links(:home)
      @page = @link.page
    end
    
    test "page_id is required" do
      refute @link.update page_id: nil
    end
    
    test "auto generate slug" do
      slug = @link.page.title.create_id
      @link.update slug: nil
      assert_equal slug, @link.slug
    end
    
    test "prevent duplicate page link at ancestry" do
      assert_raises ActiveRecord::RecordInvalid do
        @page.links.create!
      end
    end
    
    test "generate unqiue slug at ancestry" do
      link = Link.create page: frame_pages(:widget1), slug: @link.slug
      assert_equal @link.slug + '-1', link.slug
      
      link = Link.create page: frame_pages(:widget2), slug: link.slug
      assert_equal @link.slug + '-2', link.slug
    end
  end
end
