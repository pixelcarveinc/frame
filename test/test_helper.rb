# Configure Rails Environment
ENV["RAILS_ENV"] = "test"

require File.expand_path("../dummy/config/environment.rb",  __FILE__)
require "rails/test_help"

Rails.backtrace_cleaner.remove_silencers!

# Load support files
Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

# Load fixtures from the engine
ActiveSupport::TestCase.fixture_path = File.expand_path("../fixtures", __FILE__)
ActionDispatch::IntegrationTest.fixture_path = File.expand_path("../fixtures", __FILE__)

require 'database_cleaner'
require 'capybara/rails'
require 'integration/site_dsl'

# set some defaults
# Capybara.current_driver = Capybara.javascript_driver # :selenium by default
# Capybara.default_wait_time = 2
DatabaseCleaner.strategy = :deletion


class ActiveSupport::TestCase
  fixtures :all
end

class ActionController::TestCase
  include Devise::TestHelpers

  setup do
    # dynamic database driven routes need to be reloaded after loading fixtures
    Rails.application.reload_routes!
    
    @routes = Frame::Engine.routes
    sign_in users(:debbie)
  end
      
  private
  
    def assert_attributes(model, params)
      attrs = model.attributes.clone.keep_if { |k, v| params.keys.include?(k.to_sym) }
      assert_equal attrs, params.stringify_keys
    end
end

class ActionDispatch::IntegrationTest
  fixtures :all
  
  # Make the Capybara DSL available in all integration tests
  include Capybara::DSL
  # custom DSL for testing site
  include SiteDsl
  
  # use database cleaner instead of transactional fixtures
  self.use_transactional_fixtures = false
  
  setup do
    DatabaseCleaner.start
    # dynamic database driven routes need to be reloaded after loading fixtures
    Rails.application.reload_routes!
  end
  
  teardown do
    DatabaseCleaner.clean
  end
end