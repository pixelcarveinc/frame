require 'test_helper'

class BlogsAndPostsFlowsTest < ActionDispatch::IntegrationTest
  fixtures :all

  setup do
    Capybara.current_driver = Capybara.javascript_driver
    sign_in
  end
  
  test "create, update and delete blog with posts" do
    visit '/frame/links'
    
    
    # create page
    click_link 'New'
    
    within 'form' do
      fill_in 'page_title', with: 'Test Blog'
      select 'Blog', from: 'page_type'
      click_button 'Save'
    end
    
    # link page
    blog = find_page_in_context_menu('Test Blog')
    home = find_link('Home')
    blog.drag_to home.first('.dropzone.below')
    
    # edit page
    blog.find('.controls .icon-edit').click

    # should have form content menu with posts link
    find('#form_content a', text: 'POSTS').click
    
    # should have rendered post listing
    assert find '#header h2', text: 'Posts'
    
    click_link 'New'
    
    within 'form' do
      fill_in 'post_category', with: 'General'
      fill_in 'post_title', with: 'Test Post 1'
      fill_in_ck 'post_body', with: 'Body for test post 1'
      click_button 'Save'
    end
    
    # should have created post
    assert find('table.posts td.title', text: 'Test Post 1')
    
    click_link 'New'
    
    within 'form' do
      fill_in 'post_category', with: 'General'
      fill_in 'post_title', with: 'Test Post 2'
      fill_in_ck 'post_body', with: 'Body for test post 2'
      find('#post_published a.off.pseudo-radio').click
      click_button 'Save'
    end
    
    # should have created post
    assert find('table.posts td.title', text: 'Test Post 2')
    
    # should not be marked published
    assert find_listing_row('Test Post 2').find('.published .off')
    
    visit '/site'
    within('ul.level-0') { click_link 'Test Blog' }
    
    assert find('h1', text: 'Test Blog')
    
    # assert published is present and vice-versa for non-published
    assert page.has_text? 'Test Post 1'
    assert page.has_no_text? 'Test Post 2'
    
    # go back and set post 2 published
    visit '/frame/links'
    find_page_in_context_menu('Test Blog').find('.controls .icon-edit').click
    find('#form_content a', text: 'POSTS').click
    
    find_listing_row('Test Post 2').find('.controls .edit').click
    
    within 'form' do
      find('#post_published a.on.pseudo-radio').click
      click_button 'Save'
    end
    
    # should be marked published
    assert find_listing_row('Test Post 2').find('.published .on')
    
    visit '/site/test-blog'
    assert page.has_text? 'Test Post 1'
    assert page.has_text? 'Test Post 2'
    
    # change post 1's title and confirm it's accessible (routes reload)
    visit '/frame/links'
    find_page_in_context_menu('Test Blog').find('.controls .icon-edit').click
    find('#form_content a', text: 'POSTS').click
    
    find_listing_row('Test Post 1').find('.controls .edit').click
    
    within 'form' do
      fill_in 'post_title', with: 'Test New Post Slug'
      click_button 'Save'
    end
    
    # should have changed post title
    assert find('table.posts td.title', text: 'Test New Post Slug')
    
    visit '/site/test-blog'
    assert page.has_text? 'Test New Post Slug'
    assert page.has_text? 'Test Post 2'
    
    click_link 'Test New Post Slug'
    
    assert find('h2', text: 'Test New Post Slug')
    
    # TODO delete a post, confirm it's not present in listings or public site
  end
  
end
