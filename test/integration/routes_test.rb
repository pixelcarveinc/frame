require 'test_helper'

class RoutesTest < ActionDispatch::IntegrationTest
  fixtures :all

  setup do
    Capybara.current_driver = Capybara.javascript_driver
    sign_in
  end

  test "dynamic routes" do
    visit '/site'
    assert find('h1', text: 'Home')
    
    within('ul.level-0') { click_link 'Widgets' }
    assert find('h1', text: 'Widgets')
    
    within('ul.level-2') { click_link 'Widget 2' }
    assert find('h1', text: 'Widget 2')
    
    visit '/site/featured/product-1'
    assert find('h2', text: 'Product 1')
    
    visit '/site/widgets'
    assert find('h1', text: 'Widgets')
    
    click_link 'Desert'
    assert find('h2', text: 'Desert')
    
    # should not have a route
    visit '/site/widgets/pricing'
    assert page.has_text? 'Internal Server Error'
  end

  test "updates refresh routes" do
    visit '/site/featured'
    assert find('h1', text: 'Featured')
    
    # edit page
    visit '/frame/links'
    find_page_in_context_menu('Featured').find('.controls .icon-edit').click
    
    within 'form' do
      fill_in 'blog_title', with: 'Best Of'
      click_button 'Save'
    end
    
    # should have been updated
    assert find_page_in_context_menu('Best Of')
    
    visit '/site/best-of'
    assert find('h1', text: 'Best Of')
    
    # should no longer exist
    visit '/site/featured'
    assert page.has_text? 'Internal Server Error'
  end
  
  test "contact form" do
    visit '/site/contact'
    assert find('form#new_message')
    
    # fill in form with invalid email and text, assert error messages
    within '#new_message' do
      fill_in 'message_name', with: 'Ted Testman'
      fill_in 'message_phone_number', with: '(416) 555-0555'
      fill_in 'message_email_address', with: 'invalid'
      fill_in 'message_company_name', with: 'CompuGlobalHyperMegaNet'
      fill_in 'message_comments', with: '<p>Invalid</p>'
      click_button 'Send'
    end
    
    #assert find('#error_explanation li', text: 'Email address can\'t be blank')
    assert find('#error_explanation li', text: 'Comments may not contain HTML')
    
    # fill in form with valid values, assert success response on submit
    within '#new_message' do
      fill_in 'message_name', with: 'Ted Testman'
      fill_in 'message_phone_number', with: '(416) 555-0555'
      fill_in 'message_email_address', with: 'test@example.net'
      fill_in 'message_company_name', with: 'CompuGlobalHyperMegaNet'
      fill_in 'message_comments', with: 'Cool site'
      click_button 'Send'
    end
    
    assert find('#notice', text: 'Thank you, your message has been received.')
  end
  
end