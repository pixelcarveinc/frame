require 'test_helper'

class LinksAndPagesFlowsTest < ActionDispatch::IntegrationTest
  fixtures :all

  setup do
    Capybara.current_driver = Capybara.javascript_driver
    sign_in
  end
  
  test "create, update and delete page" do
    visit '/frame/links'
    
    # should have rendered page
    find '#header h2', text: 'Navigation'
    find '#context-menu .pages'
    # should have rendered tree from JS
    all 'ul.roots > li', count: 4
    
    # create page
    click_link 'New'
    # should have rendered page
    find '#header h2', text: 'New Page'
    
    within 'form' do
      fill_in 'page_title', with: 'Test Page'
      fill_in_ck 'page_body', with: 'Test body'
      click_button 'Save'
    end
    
    # edit page
    find_page_in_context_menu('Test Page').find('.controls .icon-edit').click
    
    within 'form' do
      fill_in 'page_title', with: 'Test Page 2'
      fill_in_ck 'page_body', with: 'Test body 2'
      click_button 'Save'
    end
    
    # should have been updated
    find_page_in_context_menu('Test Page 2').find('.controls .icon-edit').click
    assert_equal "<p>Test body 2</p>\n", find("#page_body", visible: false).value
    
    find('#controls .back').click
        
    # delete page
    find_page_in_context_menu('Test Page 2').find('.controls .icon-delete').click

    within_dialog { click_button 'OK' }
    
    # should have been deleted
    assert page.has_no_text? 'Test Page 2'
  end

  # TODO implement rest of test
  test "linking" do
    visit '/frame/links'
    
    # link a page
    page = find_page_in_context_menu('Widget 2')
    link_into = find_link('Featured')
    
    page.drag_to link_into.first('.dropzone.into')
    
    page.reload
    
    assert_links ['Widget 1', 'Widget 2'], find_link('Featured')
    
    # move link in siblings
    # move a link to a new parent
    # delete link
    # delete a page & all its links
  end

end

