require 'test_helper'

class GalleriesAndImagesFlowsTest < ActionDispatch::IntegrationTest
  fixtures :all

  setup do
    Capybara.current_driver = Capybara.javascript_driver
    sign_in
  end
  
  # crud gallery -> frontend verify
  # crud images -> frontend verify
  # change image position -> frontend verify
  # change image published state -> frontend verify
  # images multi-delete -> frontend verify
  
  test "create, update and delete gallery with images" do
    visit '/frame/links'
    
    # create page
    click_link 'New'
    
    within 'form' do
      fill_in 'page_title', with: 'Test Gallery'
      select 'Gallery', from: 'page_type'
      click_button 'Save'
    end
    
    # edit page
    find_page_in_context_menu('Test Gallery').find('.controls .icon-edit').click

    # should have form content menu with images link
    find('#form_content a', text: 'IMAGES').click
    
    # should have rendered image listing
    assert find '#header h2', text: 'Images'
    
    # TODO need to implement multi-upload for new images first
    # TODO implement draggable position changes for images
    
  end
end
