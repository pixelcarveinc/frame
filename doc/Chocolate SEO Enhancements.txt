Goals:
- easy to add SEO meta data
- automaticaly populates by default
- extensible, allowing for more complex types



page meta class covering various meta data that may need to be entered for pages
site settings also contains meta data, used when page doesn't specify any

Seo model (may be tied to any sort of content in system):
	- title (take from page by default)
	- description (take from HTML stripped page body by default)
	- image (optional but recommended)
	- content_type (combines types from twitter card, opengraph and schema.org - default to article)
	
SeoMeta model
	- type (meta / itemprop / itemtype)
	- name
	- content
	
Content types:

generic
article
product
photo
video
music
place
	
maybe combine twitter_card_type, opengraph_object_type and Schema.org type into one

https://dev.twitter.com/docs/cards
https://developers.facebook.com/docs/reference/opengraph
http://schema.org/docs/schemas.html

- content_type is only used if not overridden
- url is determined automatically but may be overridden
